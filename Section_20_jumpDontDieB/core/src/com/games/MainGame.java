package com.games;

import com.badlogic.gdx.ApplicationAdapter;

/*
lesson 203-205 Section 20.
Inputs. Ways to enter info through keyboard, mouse or touching the mobile screen.
listeners created: touchDown() and touchUp().
linking the inputProcessor with the controller (MainGame).
 */
public class MainGame extends ApplicationAdapter {

    @Override
    public void create() {
        /*this instance links the input processor.
        Processor p = new Processor();
        Gdx.input.setInputProcessor(p);
    }

    @Override
    public void dispose() { //this one releases resources from the graphic adapter.

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //cleans the buffer_bit of the graphics adapter.

        /*this if was used only for lesson 203 to show
        how you could enter info.
         */
        /*if (Gdx.input.justTouched()) {
            //System.out.println("estas tocando la pantalla."); //for isTouched()
            System.out.println("justo tocaste la pantalla."); //for isTouched()
        }*/
    }
}
