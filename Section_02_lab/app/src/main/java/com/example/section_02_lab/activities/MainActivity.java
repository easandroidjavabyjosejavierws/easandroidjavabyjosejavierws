package com.example.section_02_lab.activities;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.section_02_lab.R;
import com.example.section_02_lab.adapters.FruitAdapter;
import com.example.section_02_lab.models.Fruit;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    //ListView, GridView and adapters
    private ListView listView;
    private GridView gridView;
    private FruitAdapter adapterListView;
    private FruitAdapter adapterGridView;

    //ArrayList based in Fruit class
    private List<Fruit> fruits;

    // Items en el option menu
    private MenuItem itemListView;
    private MenuItem itemGridView;

    //some variables
    private int counter = 0;
    private final int SWITCH_TO_LIST_VIEW = 0;
    private final int SWITCH_TO_GRID_VIEW = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // render the app icon
        enforceIconBar();

        //call to populate the fruits arrayList.
        fruits = getAllFruits();

        //instantiate the list and grid views.
        listView = findViewById(R.id.listView);
        gridView = findViewById(R.id.gridView);

        /*implement a listener to detect when an item
        is clicked either in the list or grid view.
         */
        listView.setOnItemClickListener(this);
        gridView.setOnItemClickListener(this);

        /*the customized adapter is instantiated.
        it's the same for the list and grid views.
         */
        adapterListView = new FruitAdapter(this, R.layout.list_view_item_fruit, fruits);
        adapterGridView = new FruitAdapter(this, R.layout.grid_view_item_fruit, fruits);

        listView.setAdapter(adapterListView);
        gridView.setAdapter(adapterGridView);

        /*register the context menu for both of them.
        this is the link for the taken actions on the
        context.
         */
        registerForContextMenu(this.listView);
        registerForContextMenu(this.gridView);
    }

    private void enforceIconBar() {
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /*this method is required because I implemented
     AdapterView.OnItemClickListener class.
     it calls clickFruit() to handle the click event.
      */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.clickFruit(fruits.get(position));
    }

    /* this the event handle when a click is taken
    either the list or the grid view.
     */
    private void clickFruit(Fruit fruit) {
        // Differentiate between the known and the unknown fruits.
        if(fruit.getOrigin().equals("Unknown"))
            Toast.makeText(this, "Sorry, we don't have many info about " + fruit.getName(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "The best fruit from " + fruit.getOrigin() + " is " + fruit.getName(), Toast.LENGTH_SHORT).show();
    }

        //to render options (icons) into the action bar menu
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // the menu is inflated with our layout (option_menu.xml)
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.option_menu, menu);
            /* retrieve the button references that I need.
            these are the buttons to render in the action bar.
             */
            itemListView = menu.findItem(R.id.list_view);
            itemGridView = menu.findItem(R.id.grid_view);
            return true;
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // event handler for the clicks in each action bar options (icons)
        switch (item.getItemId()) {
            case R.id.add_fruit:
                addFruit(new Fruit("Added nº" + (++counter), R.mipmap.ic_new_fruit, "Unknown"));
                return true;
            case R.id.list_view:
                switchListGridView(this.SWITCH_TO_LIST_VIEW);
                return true;
            case R.id.grid_view:
                switchListGridView(this.SWITCH_TO_GRID_VIEW);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        // the context menu is inflated with my layout (context_menu_fruits).
        MenuInflater inflater = getMenuInflater();
        /* before to inflate, a header is added to the pop up window according
          to the clicked element. This header brings more clarification.
         */
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(this.fruits.get(info.position).getName());
        // Inflate
        inflater.inflate(R.menu.context_menu_fruits, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        /* retrive info of clicked element of the
        context menu.
        the only action I am taking is to delete the clicked element
         */
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.delete_fruit:
                deleteFruit(info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /*populating the list with objects type Fruit.
    uses the Fruit constructor.
     */
    private List<Fruit> getAllFruits() {
        List<Fruit> list = new ArrayList<Fruit>() {{
            add(new Fruit("Banana", R.mipmap.ic_banana, "Gran Canaria"));
            add(new Fruit("Strawberry", R.mipmap.ic_strawberry, "Huelva"));
            add(new Fruit("Orange", R.mipmap.ic_orange, "Sevilla"));
            add(new Fruit("Apple", R.mipmap.ic_apple, "Madrid"));
            add(new Fruit("Cherry", R.mipmap.ic_cherry, "Galicia"));
            add(new Fruit("Pear", R.mipmap.ic_pear, "Zaragoza"));
            add(new Fruit("Raspberry", R.mipmap.ic_raspberry, "Barcelona"));
        }};
        return list;
    }

    private void switchListGridView(int option) {
        /* this one handles the rendering between
        the Grid and List view.
        by default the grid view is hidden when the app starts.
         */
        if (option == SWITCH_TO_LIST_VIEW) {
            /* if the list view is hidden and I want to render it.
             */
            if (this.listView.getVisibility() == View.INVISIBLE) {
                /* the grid view is hidden and its button is rendered
                in the action bar.
                 */
                this.gridView.setVisibility(View.INVISIBLE);
                this.itemGridView.setVisible(true);
                /* the list view is set up to visible and its button
                is hidden in the action bar.
                 */
                this.listView.setVisibility(View.VISIBLE);
                this.itemListView.setVisible(false);
            }
        } else if (option == SWITCH_TO_GRID_VIEW) {
            /* if the grid view is hidden and I want to render it.
             */
            if (this.gridView.getVisibility() == View.INVISIBLE) {
                /* the list view is hidden and its button is rendered
                in the action bar.
                 */
                this.listView.setVisibility(View.INVISIBLE);
                this.itemListView.setVisible(true);
                /* the grid view is set up to visible and its button
                is hidden in the action bar.
                 */
                this.gridView.setVisibility(View.VISIBLE);
                this.itemGridView.setVisible(false);
            }
        }
    }

    // some CRUD actions - create and delete.
    private void addFruit(Fruit fruit) {
        this.fruits.add(fruit);
        // notify these adds to the adapters.
        adapterListView.notifyDataSetChanged();
        adapterGridView.notifyDataSetChanged();
    }

    private void deleteFruit(int position) {
        this.fruits.remove(position);
        // notify these deletions to the adapters.
        adapterListView.notifyDataSetChanged();
        adapterGridView.notifyDataSetChanged();
    }
}