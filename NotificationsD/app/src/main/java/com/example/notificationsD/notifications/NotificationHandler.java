package com.example.notificationsD.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.os.Build;

import com.example.notificationsD.R;
import com.example.notificationsD.activities.DetailsActivity;

public class NotificationHandler extends ContextWrapper {
    private NotificationManager manager;
    public static final String CHANNEL_HIGH_ID = "1";
    private final String CHANNEL_HIGH_NAME = "HIGH CHANNEL";
    public static final String CHANNEL_LOW_ID = "2";
    private final String CHANNEL_LOW_NAME = "LOW CHANNEL";
    private final int SUMMARY_GROUP_ID = 1001; //any number
    private final String SUMMARY_GROUP_NAME = "GROUPING_NOTIFICATON"; //any name

    public NotificationHandler(Context context) {
        super(context);
        createChannel();
    }

    public NotificationManager getManager() {
        if (manager == null) {
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }

    private void createChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            //defining the high channel.
            NotificationChannel highChannel = new NotificationChannel(CHANNEL_HIGH_ID,
                                                                    CHANNEL_HIGH_NAME,
                                                                    NotificationManager.IMPORTANCE_HIGH);
            /*
            the NotificationManager.IMPORTANCE_HIGH argument impact the way that
            the notification is rendered:
            0: notification is created but not rendered. Just it creates an icon
               inside of the toolbar.
             */
            //********extra configuration *********
            highChannel.enableLights(true);
            highChannel.setLightColor(Color.YELLOW);

            highChannel.setShowBadge(true);

            highChannel.enableVibration(true);
            //highChannel.setVibrationPattern(new long[] {100, 200, 300, 400, 500, 400, 300, 200, 400});
            //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            //highChannel.setSound(defaultSoundUri, null);

            highChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);


            //defining the low channel.
            NotificationChannel lowChannel = new NotificationChannel(CHANNEL_LOW_ID,
                                                                    CHANNEL_LOW_NAME,
                                                                    NotificationManager.IMPORTANCE_LOW);
            lowChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            //creating the channels
            getManager().createNotificationChannel(highChannel);
            getManager().createNotificationChannel(lowChannel);
        }
    }

    public Notification.Builder createNotification(String title, String message, boolean isHighImportance) {
        if (Build.VERSION.SDK_INT >= 26) {
            if (isHighImportance) {
                return this.createNotificationWithChannel(title, message, CHANNEL_HIGH_ID);
            }
            return this.createNotificationWithChannel(title, message, CHANNEL_LOW_ID);
        }
        return this.createNotificationWithoutChannel(title, message);
    }

    private Notification.Builder createNotificationWithChannel(String title, String message, String channelId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //define the intent
            Intent intent = new Intent(this, DetailsActivity.class);
            //define the info to set with the intent.
            intent.putExtra("title", title);
            intent.putExtra("message", message);
            //these flags leave the app (but it doesn't close it) when the user clicks the back button.
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //create the pending intent.
            PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            Notification.Action action =
                    new Notification.Action.Builder(Icon.createWithResource(
                            this, android.R.drawable.ic_menu_send),
                            "See details", pIntent).build();
            return new Notification.Builder(getApplicationContext(), channelId)
                            .setContentTitle(title)
                            .setContentText(message)
                            //.addAction(action)
                            .setColor(getColor(R.color.colorPrimary))
                            .setSmallIcon(android.R.drawable.stat_notify_chat)
                            .setGroup(SUMMARY_GROUP_NAME)
                            .setAutoCancel(true);
        }
        return null;
    }

    private Notification.Builder createNotificationWithoutChannel(String title, String message) {
        return new Notification.Builder(getApplicationContext())
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(android.R.drawable.stat_notify_chat)
                .setAutoCancel(true);
    }

    public void publishNotificationsSummaryGroup(boolean isHighImportance) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = (isHighImportance) ? CHANNEL_HIGH_ID : CHANNEL_LOW_ID;
            Notification summaryNotification = new Notification.Builder(
                    getApplicationContext(), channelId)
                    .setSmallIcon(android.R.drawable.stat_notify_call_mute)
                    .setGroup(SUMMARY_GROUP_NAME)
                    .setGroupSummary(true)
                    .build();
            getManager().notify(SUMMARY_GROUP_ID, summaryNotification);
        }
    }
}
