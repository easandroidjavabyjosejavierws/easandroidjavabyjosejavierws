package com.example.section_06_fragmentsb.activities;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.example.section_06_fragmentsb.R;
import com.example.section_06_fragmentsb.fragments.DataFragment;
import com.example.section_06_fragmentsb.fragments.DetailsFragment;

/* lesson 106 section 9.
working with fragments, adding a tablet.
adding layouts for phone and table in portrait and landscape mode.
 */
public class MainActivity extends FragmentActivity implements DataFragment.DataListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void sendData(String text) {
        //instantiate the details fragment.
        DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.detailsFragment);
        //pass the info from data fragment to details fragment.
        detailsFragment.renderText(text);
    }
}