package com.example.section_01d1;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textView = findViewById(R.id.textView_Main);
        //recover the data from the intent.
        Bundle bundle = getIntent().getExtras();
        if (bundle != null &&
            bundle.getString("greeter") != null) {
            String importedString = bundle.getString("greeter");
            Toast.makeText(SecondActivity.this,
                    importedString,
                        Toast.LENGTH_LONG).show();
            textView.setText(importedString);
        } else {
            Toast.makeText(SecondActivity.this,
                    "the imported data is empty",
                    Toast.LENGTH_LONG).show();
        }
    }
}
