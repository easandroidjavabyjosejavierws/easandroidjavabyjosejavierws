package com.example.section_08_navigation_drawerB.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.section_08_navigation_drawerB.R;
import com.example.section_08_navigation_drawerB.fragments.AlertsFragment;
import com.example.section_08_navigation_drawerB.fragments.EmailFragment;
import com.example.section_08_navigation_drawerB.fragments.InfoFragment;
import com.google.android.material.navigation.NavigationView;

/*
lesson 123 section 11.
removed duplicated code in onCreate() and setFragmentByDefault()
and put it in a new method: changeFragment().
new listener and handler to check when the navigation view
is open or closed.
a new style was added for apis 21 and up. this functionality
extends the header to fulfill the whole screen using a transparent
property to leave the icons on top totally visible.
 */
public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbar();

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navView);

        //render a default fragment when the app opens.
        setFragmentByDefault();

        /*lister and handler that handle when the navigation view
        is open or closed.
         */
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                Toast.makeText(MainActivity.this, "open", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                Toast.makeText(MainActivity.this, "closed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                boolean fragmentTransaction = false;
                Fragment fragment = null;

                switch (item.getItemId()) {
                    case R.id.menu_email:
                        fragment = new EmailFragment();
                        fragmentTransaction = true;
                        break;
                    case R.id.menu_alert:
                        fragment = new AlertsFragment();
                        fragmentTransaction = true;
                        break;
                    case R.id.menu_info:
                        fragment = new InfoFragment();
                        fragmentTransaction = true;
                        break;
                    case R.id.menu_option_1:
                        Toast.makeText(MainActivity.this, "option 1 was clicked", Toast.LENGTH_SHORT).show();
                        break;
                }

                if (fragmentTransaction) { //this is only for the main options.
                    changeFragment(fragment, item);
                    drawerLayout.closeDrawers();
                }
                return true;
            }
        });
    }

    private void setToolbar() {
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        //inserting and rendering the hamburger icon.
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /*
    this method sets the first option as default when
    the app opens.
     */
    private void setFragmentByDefault() {
        changeFragment(new EmailFragment(), navigationView.getMenu().getItem(0));
    }

    /*this method avoids duplicated code in setFragmentByDefault()
    and onCreate().
     */
    private void changeFragment(Fragment fragment, MenuItem item) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
        item.setChecked(true); //highlights the selected option
        getSupportActionBar().setTitle(item.getTitle()); /*sets a title on
                                                          the action bar*/
    }

    /*to activate the navigation view using the
    hamburger icon.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //open the navigation view (sidebar equivalent).
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}