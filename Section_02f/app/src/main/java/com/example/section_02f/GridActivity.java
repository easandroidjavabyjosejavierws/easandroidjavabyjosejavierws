package com.example.section_02f;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/*lesson 55 section 5.
    the idea is to update grid or list view content
     working with the context menu (ie. delete an element inside of the context).
 */
public class GridActivity extends AppCompatActivity {

    private GridView gridView;
    List<String> names = new ArrayList<>();
    private int counter = 0;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        gridView = findViewById(R.id.gridView);

        //data to render is added to the listView
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");

        //add listener to the listView using lambdas:
        gridView.setOnItemClickListener((adapterView, view, position, id) -> {
            Toast.makeText(GridActivity.this, names.get(position) + " was clicked.", Toast.LENGTH_LONG).show();
        });

        //works with a customized adapter.
        myAdapter = new MyAdapter(this, R.layout.grid_item, names);
        gridView.setAdapter(myAdapter);

        registerForContextMenu(gridView);
    }

    //the option menu layout is inflated.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    //click event for the option menu.
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            //a new name is added.
            case R.id.add_item:
                /*our list gets a new item identified by
                "new item: " + (++counter)
                 */
                this.names.add("new item: " + (++counter));
                //the adapter is notified about this change
                this.myAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //the context menu layout is inflated.
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(this.names.get(info.position));
        inflater.inflate(R.menu.context_menu, menu);
    }
    //click event for the context menu.
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        /* in order to see this functionality, I need to click the desired
        item and hold it for a few microseconds until a pop up window
        opens.
        To remove an item from the collection requires knowing its position.
        This info is in the adapter and it's unknown in this activity.
        but it's possible to apply the getMenuInfo() of the item object and
        store it in an instance of the AdapterView.AdapterContextMenuInfo.
         */
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            //a clicked item must be removed.
            case R.id.delete_item:
                this.names.remove(info.position);
                //the adapter is notified about this change
                this.myAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
