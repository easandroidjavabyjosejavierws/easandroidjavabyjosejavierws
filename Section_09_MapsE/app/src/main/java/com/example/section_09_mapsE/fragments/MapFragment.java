package com.example.section_09_mapsE.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.section_09_mapsE.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapFragment extends Fragment implements OnMapReadyCallback,
                                            GoogleMap.OnMarkerDragListener,
                                            View.OnClickListener {
    private View rootView;
    private MapView mapView;
    private GoogleMap gMap;
    private List<Address> addresses;
    private Geocoder geocoder;
    private MarkerOptions myMarker;
    private FloatingActionButton fab;

    public MapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_map, container, false);

        fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(this);

        return rootView;
    }

    /*this method is executed after the view is loaded
    (the fragment has been created).
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = rootView.findViewById(R.id.map);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    /*@Override
    public void onResume() {
        super.onResume();
    }*/

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        LatLng myHome = new LatLng(26.6072037, -80.1609351);

        //code to customize a marker.
        myMarker = new MarkerOptions();
        myMarker.draggable(true);
        myMarker.position(myHome);
        myMarker.title("Hello from this place");
        myMarker.snippet("Additional info");
        myMarker.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.star_on));
        //gMap.addMarker(myMarker);
        Marker m = gMap.addMarker(myMarker);
        geocoder = new Geocoder(getContext(), Locale.getDefault());
        renderCurrentAddress(m);

        //to improve the zoom.
        CameraUpdate zoom = CameraUpdateFactory.newLatLngZoom(myHome, 12f);
        gMap.animateCamera(zoom);

        /* according to lesson 133, the zoom doesn't work
        I commented the proposed code; didn't use moveCamera() and used
        CameraUpdateFactory.newLatLngZoom().
        //CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);
        //gMap.moveCamera(CameraUpdateFactory.newLatLng(myHome));
        //gMap.animateCamera(zoom);
         */
        //dragging and dropping the current marker.
        gMap.setOnMarkerDragListener(this);

        geocoder = new Geocoder(getContext(), Locale.getDefault());
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        marker.hideInfoWindow();
    }

    @Override
    public void onMarkerDrag(Marker marker) {
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        renderCurrentAddress(marker);
    }

    private void renderCurrentAddress(Marker marker) {
        float latitude = (float) marker.getPosition().latitude;
        float longitude = (float) marker.getPosition().longitude;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String address = addresses.get(0).getAddressLine(0);
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String zip = addresses.get(0).getPostalCode();

        marker.setSnippet(address + "\n" +
                "city: " + city + "\n" +
                "state: " + state + "\n" +
                "zip: " + zip + "\n");
        //marker.showInfoWindow();
    }

    private void isLocationOn() {
        try {
            int gpsSignal = Settings.Secure.getInt(getActivity().getContentResolver(), Settings.Secure.LOCATION_MODE);
            //Settings.Secure.LOCATION_MODE is deprecated.
            if (gpsSignal == 0) {
                //location is not active.
                showInfoAlert();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void showInfoAlert() {
        new AlertDialog.Builder(getContext())
                .setTitle("Location status")
                .setMessage("Location is not active. Would you like to activate it?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", null)
                .show();
    }
    @Override
    public void onClick(View v) {
        //checking if the location is active.
        isLocationOn();
    }
}