package com.example.section_09_mapsE.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.section_09_mapsE.R;
import com.example.section_09_mapsE.fragments.MapFragment;
import com.example.section_09_mapsE.fragments.WelcomeFragment;

/*
lesson 136-137 section 12.
working with location.
Checking if location is active (in a primitive way).
Improved the location check.
 */
public class MainActivity extends AppCompatActivity {
    Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*anytime the device is rotated, this method is called.
        the next Toast.makeText() was implemented to prove it.
        Toast.makeText(this, "Called", Toast.LENGTH_SHORT).show();
         */

        //call a default frame. (only the first this app starts).
        if (savedInstanceState == null) {
            currentFragment = new WelcomeFragment();
            changeFragment(currentFragment);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_welcome:
                currentFragment = new WelcomeFragment();
                break;
            case R.id.menu_map:
                currentFragment = new MapFragment();
                break;
        }
        changeFragment(currentFragment);
        return super.onOptionsItemSelected(item);
    }

    private void changeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment).commit();
    }
}