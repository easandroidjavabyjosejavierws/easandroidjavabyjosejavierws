package com.example.section_01_lab;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    //ui elements
    private SeekBar seekBarAge;
    private TextView textViewAge;
    private Button btnNext;
    private RadioButton radioButtonGreeter;
    private RadioButton radioButtonFarewell;

    //additional data
    private String name       = "";
    private int age           = 18;
    private final int MAX_AGE = 60;
    private final int MIN_AGE = 16;

    //to share
    public static final int GREETER_OPTION  = 1;
    public static final int FAREWELL_OPTION = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //retrieve the entered name from previous activity
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            name = bundle.getString("name");
        }

        // element instantiations
        seekBarAge          = findViewById(R.id.seekBarAge);
        textViewAge         = findViewById(R.id.textViewCurrentAge);
        btnNext             = findViewById(R.id.buttonToThirdActivity);
        radioButtonGreeter  = findViewById(R.id.radioButtonGreeter);
        radioButtonFarewell = findViewById(R.id.radioButtonFarewell);

        //seekbar change event
        seekBarAge.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int currentAge, boolean b) {
                age = currentAge;
                textViewAge.setText(age + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //not implemented
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //validate entered age
                age = seekBar.getProgress();
                textViewAge.setText(age + "");

                if (age > MAX_AGE) {
                    btnNext.setVisibility(View.INVISIBLE);
                    Toast.makeText(SecondActivity.this, "The maximum age allowed is: "+ MAX_AGE +" years old.", Toast.LENGTH_LONG).show();
                } else if (age < MIN_AGE) {
                    btnNext.setVisibility(View.INVISIBLE);
                    Toast.makeText(SecondActivity.this, "The minimum age allowed is: "+ MIN_AGE +" years old.", Toast.LENGTH_LONG).show();
                } else {
                    btnNext.setVisibility(View.VISIBLE);
                }
            }
        });

        //click event to send info to the third activity, the last one.
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("age", age);
                /*if the greeter radio button was selected, the option will be 1
                    else, it will be 2
                 */
                int option = (radioButtonGreeter.isChecked()) ? GREETER_OPTION : FAREWELL_OPTION;
                intent.putExtra("option", option);
                startActivity(intent);
                /*in total I am sending three values to the last activity:
                name, age, and option.
                 */
            }
        });
    }
}
