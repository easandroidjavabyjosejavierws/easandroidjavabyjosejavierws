package com.example.section_01_lab;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
/*covers lesson 47 section 4.

 */
public class MainActivity extends AppCompatActivity {

    // UI elements
    private EditText editTextName;
    private Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set the logo for all android versions.
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // element instantiations
        editTextName = findViewById(R.id.editTextName);
        btnNext      = findViewById(R.id.buttonToSecondActivity);

        // click button event to go to the next Activity.
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                if (name != null &&
                    !name.isEmpty()) {
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("name", name);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "The name is mandatory, please enter it", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
