package com.example.httprequestE.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequestE.R;
import com.example.httprequestE.models.Town;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/*
lesson 176 section 18.
using Gson object from google and parsing
json with embedded collections.
Implemented an annotation to rename the json properties names.

 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String json =
                "{" +
                    "id: 0," +
                    "ciudades: [" +
                                "{" +
                                    "id: 8,"+
                                    "name: 'Lake Worth'" +
                                "}," +
                                "{" +
                                    "id: 2,"+
                                    "name: 'Delray Beach'" +
                                "}]" +
                "}";

        Gson gson = new GsonBuilder().create();
        Town town = gson.fromJson(json, Town.class);
        //Toast.makeText(this, town.getCity().getId() + " -- " + town.getCity().getName(), Toast.LENGTH_LONG).show();
    }
}