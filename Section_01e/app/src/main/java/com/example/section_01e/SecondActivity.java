package com.example.section_01e;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    private TextView textView;
    private Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textView = findViewById(R.id.textView_Main);
        btnNext = findViewById(R.id.buttonGoSharing);
        //recover the data from the intent.
        Bundle bundle = getIntent().getExtras();
        if (bundle != null &&
            bundle.getString("greeter") != null) {
            String importedString = bundle.getString("greeter");
            Toast.makeText(SecondActivity.this,
                    importedString,
                        Toast.LENGTH_LONG).show();
            textView.setText(importedString);
        } else {
            Toast.makeText(SecondActivity.this,
                    "the imported data is empty",
                    Toast.LENGTH_LONG).show();
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calls the third activity.
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                startActivity(intent);
            }
        });
    }
}
