package com.example.section_01e;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    /* covers lesson 38, 39, 40, 44 and 45 section 4.
    this pretends to work with implicit intent.
    */
    private Button btn;
    private final String GREETER = "hello from the other side";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.button_Main);
        //findViewById() returns a View object (a component).
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calls the second activity, sending a string.
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                //intent allows to send  data to other activities.
                intent.putExtra("greeter", GREETER);
                startActivity(intent);
            }
        });
    }
}
