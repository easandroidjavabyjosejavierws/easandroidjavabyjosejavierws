package com.example.notificationsB.activities;

import android.app.Notification;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.example.notificationsB.R;
import com.example.notificationsB.notifications.NotificationHandler;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/*
lesson 191 section 19.
uses a pendingIntent. This kind of intent is defined with the notification, but the user
could cancel the notification or proceed to click on the notification and then it sends
info to new activity as a regular intent.
 */
public class MainActivity extends AppCompatActivity {
    @BindView(R.id.editTextTitle)
    EditText editTextTitle;
    @BindView(R.id.editTextMessage)
    EditText editTextMessage;
    @BindView(R.id.switchImportance)
    Switch switchImportance;
    @BindString(R.string.switch_notifications_on) String switchTextOn;
    @BindString(R.string.switch_notifications_off) String switchTextOff;
    private boolean isHighImportance = false;
    private NotificationHandler notificationHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this); //right after setContentView.
        notificationHandler = new NotificationHandler(this);
    }

    @OnClick(R.id.buttonSend)
    public void click() {
        sendNotification();
    }

    @OnCheckedChanged(R.id.switchImportance)
    public void change(CompoundButton buttonView, boolean isChecked) {
        isHighImportance = isChecked;
        //Toast.makeText(this, "Working: " + isHighImportance, Toast.LENGTH_LONG).show();
        switchImportance.setText((isChecked) ? switchTextOn : switchTextOff);
    }
    private void sendNotification() {
        //Toast.makeText(this, "Working", Toast.LENGTH_LONG).show();
        String title = editTextTitle.getText().toString();
        String message = editTextMessage.getText().toString();

        if (!TextUtils.isEmpty(title) &&    //isEmpty() checks if title was entered.
            !TextUtils.isEmpty(message)) {
            Notification.Builder nb = notificationHandler.createNotification(title, message, isHighImportance);
            notificationHandler.getManager().notify(1, nb.build()); //this triggers the notification.
        }
    }
}