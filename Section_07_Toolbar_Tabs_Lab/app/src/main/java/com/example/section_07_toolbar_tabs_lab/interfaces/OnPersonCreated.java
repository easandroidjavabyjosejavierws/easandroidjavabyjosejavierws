package com.example.section_07_toolbar_tabs_lab.interfaces;

import com.example.section_07_toolbar_tabs_lab.models.Person;

/*
this interface allows the communication between the fragments,
through the main activity that must implement the createPerson();
 */
public interface OnPersonCreated {
    void createPerson(Person person);
}
