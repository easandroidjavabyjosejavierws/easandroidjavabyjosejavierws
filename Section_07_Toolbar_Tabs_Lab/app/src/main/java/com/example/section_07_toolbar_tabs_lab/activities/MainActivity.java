package com.example.section_07_toolbar_tabs_lab.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.example.section_07_toolbar_tabs_lab.R;
import com.example.section_07_toolbar_tabs_lab.adapters.ViewPagerAdapter;
import com.example.section_07_toolbar_tabs_lab.fragments.PersonListFragment;
import com.example.section_07_toolbar_tabs_lab.interfaces.OnPersonCreated;
import com.example.section_07_toolbar_tabs_lab.models.Person;
import com.google.android.material.tabs.TabLayout;

/*
lesson 115, section 10.
this is a main activity with two tabs: it requires two fragments
and communication between them.
 */
public class MainActivity extends AppCompatActivity implements OnPersonCreated {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    //constants.
    // fragment id inside of the viewPager adapter.
    public static final int PERSON_FORM_FRAGMENT = 0;
    public static final int PERSON_LIST_FRAGMENT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbar();
        setTabLayout();
        setViewPager();
        setListenerTabLayout(viewPager);
    }

    private void setToolbar() {
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
    }

    private void setTabLayout() {
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Form"));
        tabLayout.addTab(tabLayout.newTab().setText("List"));
    }

    private void setViewPager() {
        viewPager = findViewById(R.id.viewPager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), this, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void setListenerTabLayout(final ViewPager viewPager) {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void createPerson(Person person) {
        /*
        instantiate the list fragment using getSupportFragmentManager().
         */
        PersonListFragment listFragment = (PersonListFragment) getSupportFragmentManager().getFragments().get(PERSON_LIST_FRAGMENT);
        //run the addPerson() from this instance.
        listFragment.addPerson(person);
        //move the viewPager to the list fragment to see the addition.
        viewPager.setCurrentItem(PERSON_LIST_FRAGMENT);
    }
}