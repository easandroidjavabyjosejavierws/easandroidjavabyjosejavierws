package com.example.section_07_toolbar_tabs_lab.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.section_07_toolbar_tabs_lab.R;
import com.example.section_07_toolbar_tabs_lab.interfaces.OnPersonCreated;
import com.example.section_07_toolbar_tabs_lab.models.Country;
import com.example.section_07_toolbar_tabs_lab.models.Person;
import com.example.section_07_toolbar_tabs_lab.utils.Util;

import java.util.List;

public class PersonFormFragment extends Fragment {
    private EditText editTextName;
    private Spinner spinnerCountry;
    private Button btnCreate;
    private List<Country> countries;
    private OnPersonCreated onPersonCreated;

    public PersonFormFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_person_form, container, false);

        editTextName = view.findViewById(R.id.editTextName);
        spinnerCountry = view.findViewById(R.id.spinnerCountry);
        btnCreate = view.findViewById(R.id.buttonCreate);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewPerson();
            }
        });

        //getting countries
        countries = Util.getCountries();

        /*
        an arrayAdapter is created to be passed to the spinner.
        the process implies an adapter and this one requires a layout,
        we will use an out of the pocket layout: simple_spinner_dropdown_item.
         */
        ArrayAdapter<Country> adapter = new ArrayAdapter<Country>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);
        spinnerCountry.setAdapter(adapter);
        return view;
    }

    private void createNewPerson() {
        if (!editTextName.getText().toString().isEmpty()) {
            //retrieve the selected country:
            Country country = (Country) spinnerCountry.getSelectedItem();
            Person person = new Person(editTextName.getText().toString(), country);
            //use the interface to pass info to the list fragment.
            onPersonCreated.createPerson(person);
        } else {
            Toast.makeText(getContext(), "Person name is required", Toast.LENGTH_SHORT).show();
        }
    }
    /*event to link to the listener.
     */

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnPersonCreated) {
            onPersonCreated = (OnPersonCreated) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnPersonCreated");
        }
    }

    /*event to detach the listener.
     */

    @Override
    public void onDetach() {
        super.onDetach();
        onPersonCreated = null;
    }
}