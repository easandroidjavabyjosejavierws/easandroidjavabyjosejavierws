package com.example.section_01d2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    /* covers lesson 39 section 4.
    second activity calls third activity.
    phone call ask for permission. If allowed its stored
    and makes the call.
    if denied send a message.
    */
    private Button btn;
    private final String GREETER = "hello from the other side";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.button_Main);
        //findViewById() returns a View object (a component).
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MainActivity.this,
//                        "main activity calling second one",
//                        Toast.LENGTH_LONG).show();
                //calls the second activity, sending a string.
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                //intent allows to send  data to other activities.
                intent.putExtra("greeter", GREETER);
                startActivity(intent);
            }
        });
    }
}
