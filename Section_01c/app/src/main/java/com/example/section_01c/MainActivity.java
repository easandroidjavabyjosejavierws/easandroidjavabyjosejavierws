package com.example.section_01c;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
/* covers lesson 36 section 4.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /* this simply renders (uses Toast) a message
        working code, without using the button property.
        it implements View.OnClickListener.
        this only works for one listener (it's much better
        to work with section_01b, but the code is less readable).
    */
    private View btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.button_Main);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
                Toast.makeText(MainActivity.this,
                        "button clicked using code implementing View.OnClickListener",
                        Toast.LENGTH_LONG).show();
    }
}
