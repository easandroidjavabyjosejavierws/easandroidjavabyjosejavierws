package com.example.section_01f;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class ThirdActivity extends AppCompatActivity {
    private EditText editTextPhone;
    private EditText editTextWeb;
    private ImageButton imgBtnPhone;
    private ImageButton imgBtnWeb;
    private ImageButton imgBtnCamera;

    private final int PHONE_CALL_CODE       = 100;
    private final int PICTURE_FROM_CAMERA   = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        //activate back option
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        editTextPhone = findViewById(R.id.editTextPhone);
        editTextWeb = findViewById(R.id.editTextWeb);
        imgBtnPhone = findViewById(R.id.imageButtonPhone);
        imgBtnWeb = findViewById(R.id.imageButtonWeb);
        imgBtnCamera = findViewById(R.id.imageButtonCamera);

        //phone call button
        imgBtnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editTextPhone.getText().toString();
                if (phoneNumber != null   &&
                    !phoneNumber.isEmpty()) {
                    //check the current android version that we are running.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        //check if the user had accepted, had not or never bean asked
                        if (checkPermission(Manifest.permission.CALL_PHONE)) {
                            //the user had accepted
                            Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                            if (ActivityCompat.checkSelfPermission(ThirdActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            startActivity(i);
                        } else {
                            //the user didn't accepted or it was the first time been asked
                            if (!shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                                //the user had not been asked
                                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                            } else {
                                //the user didn't accept permission
                                Toast.makeText(ThirdActivity.this, "Enable the permission, please", Toast.LENGTH_LONG).show();
                                //show the permissions from the settings
                                Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                i.addCategory(Intent.CATEGORY_DEFAULT);
                                i.setData(Uri.parse("package:" + getPackageName()));
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                startActivity(i);
                            }
                        }
                    } else {
                        olderVersions(phoneNumber);
                    }
                } else {
                    Toast.makeText(ThirdActivity.this, "insert a valid phone number", Toast.LENGTH_LONG).show();
                }
            }

            private void olderVersions(String phoneNumber) {
                Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                if (checkPermission(Manifest.permission.CALL_PHONE)) {
                    startActivity(intentCall);
                } else {
                    Toast.makeText(ThirdActivity.this, "access denied", Toast.LENGTH_LONG).show();
                }
            }
        });

        //web browser button
        imgBtnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = editTextWeb.getText().toString();
                String email = "orlarias99@gmail.com";
                if (url != null &&
                    !url.isEmpty()) {
                    Intent intentWeb = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" +url));
                    //another way to build the previous statement
//                    Intent intentWeb = new Intent();
//                    intentWeb.setAction(Intent.ACTION_VIEW);
//                    intentWeb.setData(Uri.parse("http://" +url));
                    //startActivity(intentWeb);

                    //contacts
                    Intent intentContacts = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));
                    //startActivity(intentContacts);

                    //fast email
                    Intent intentMailTo = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" +email));
                    //startActivity(intentMailTo);

                    //full email
                    //Intent intentMail = new Intent(Intent.ACTION_VIEW, Uri.parse(email));
                    /*note: the setClassName() from the course (the commented next one, does'nt work).
                        with new statement, i don't have an exception, but the wanted functionality, the one with
                        the putExtra() methods doesn't work.
                     */
                    //intentMail.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail"); //original from course
//                    intentMail.setClassName("com.google.android.gm", "com.google.android.gm.ConversationListActivityGmail");
//                    intentMail.setType("plain/text");
//                    intentMail.putExtra(Intent.EXTRA_SUBJECT, "mail title");
//                    intentMail.putExtra(Intent.EXTRA_TEXT, "hi there, i love myForm app, but...");
//                    intentMail.putExtra(Intent.EXTRA_EMAIL, new String [] {"fernando@gmail.com", "antonio@gmail.com"});
//                    startActivity(intentMail);
                    //this code partially works
//                    try {
//                        Intent intentMail = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + email));
//                        intentMail.putExtra(Intent.EXTRA_SUBJECT, "mail title");
//                        intentMail.putExtra(Intent.EXTRA_TEXT, "hi there, i love myForm app, but...");
//                        intentMail.putExtra(Intent.EXTRA_EMAIL, new String[] {"fernando@gmail.com", "antonio@gmail.com"});
//                        startActivity(intentMail);
//                    } catch(Exception e) {
//                        Toast.makeText(ThirdActivity.this, "Sorry...You don't have any mail app", Toast.LENGTH_LONG).show();
//                        e.printStackTrace();
//                    }

                    //another way to do the previous code
                    Intent intentMail = new Intent(Intent.ACTION_SEND, Uri.parse(email));
                    intentMail.setType("plain/text");
                    intentMail.putExtra(Intent.EXTRA_SUBJECT, "mail title");
                    intentMail.putExtra(Intent.EXTRA_TEXT, "hi there, i love myForm app, but...");
                    intentMail.putExtra(Intent.EXTRA_EMAIL, new String[] {"fernando@gmail.com", "antonio@gmail.com"});
                    //startActivity(Intent.createChooser(intentMail, "pick an email service"));

                    //phone number 2 without permissions
                    Intent intentPhone = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:5615436495"));
                    startActivity(intentPhone);
                }
            }
        });

        //open a camera. (it doesn't open the camera).
        imgBtnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intentCamera = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(intentCamera);
                //another try
//                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivity(intentCamera);

                //take a picture and store it. doesn't work
                Intent intentCamera = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intentCamera, PICTURE_FROM_CAMERA);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case PICTURE_FROM_CAMERA:

                if (resultCode == Activity.RESULT_OK) {
                    String result = data.toUri(0);
                    Toast.makeText(this, "result: " + result, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "there was an error with the picture, try again.", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //this the phone situation
        switch (requestCode) {
            case PHONE_CALL_CODE:
                String permission   = permissions[0];
                int result          = grantResults[0];

                if (permission.equals(Manifest.permission.CALL_PHONE)) {
                    //check if the permission was accepted
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        //permission accepted
                        String phoneNumber = editTextPhone.getText().toString();
                        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +phoneNumber));
                        //the following it is written by the assistent wizard.
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(intentCall);
                    } else {
                        //permission denied
                        Toast.makeText(ThirdActivity.this, "access denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean checkPermission(String permission) {
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}
