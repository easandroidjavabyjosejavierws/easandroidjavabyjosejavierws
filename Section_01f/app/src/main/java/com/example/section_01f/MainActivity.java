package com.example.section_01f;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    /* covers lesson 46 section 4.
    Render an icon into the action bar and for the phone app id. Change the app name.
    Implement back arrow in the secondary activities.
    */
    private Button btn;
    private final String GREETER = "hello from the other side";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set the icon into the action bar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_myicon);

        btn = findViewById(R.id.button_Main);
        //findViewById() returns a View object (a component).
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calls the second activity, sending a string.
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                //intent allows to send  data to other activities.
                intent.putExtra("greeter", GREETER);
                startActivity(intent);
            }
        });
    }
}
