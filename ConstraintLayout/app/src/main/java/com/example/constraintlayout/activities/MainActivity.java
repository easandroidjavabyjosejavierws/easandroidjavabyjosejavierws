package com.example.constraintlayout.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.constraintlayout.R;
/*
lesson 164-169 section 17.
constraint layout practice.
issues with long text in textView when the phone was
in a landscape mode. The constraint layout
was nested in a ScrollView to fix this issue.
use of the baseline to constrain one textView
to another textView baseline.

 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}