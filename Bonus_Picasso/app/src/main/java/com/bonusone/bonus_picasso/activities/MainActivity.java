package com.bonusone.bonus_picasso.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bonusone.bonus_picasso.R;
import com.bonusone.bonus_picasso.adapters.AnimalAdapter;
import com.bonusone.bonus_picasso.adapters.ImagesAdapter;
import com.bonusone.bonus_picasso.adapters.PartyAdapter;

import java.util.ArrayList;
import java.util.List;

/*
lesson 158 section 15
refactoring the package name.
com.example is not acceptable when the app
is sent to the google store.
 */
public class MainActivity extends AppCompatActivity {
    private List<String> images; //images loaded into the phone.
    private String[] animals; //images accessed through external links.
    private int[] parties; //images loaded into the app (drawable).
    private AnimalAdapter animalAdapter;
    private PartyAdapter partyAdapter;
    private ImagesAdapter imagesAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private final int PERMISSION_READ_EXTERNAL_MEMORY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        animals = getAnimalsLinks();
        parties = getPartyPics();
        images = getImagesPath();

        animalAdapter = new AnimalAdapter(this, animals, R.layout.image_layout);
        partyAdapter = new PartyAdapter(this, parties, R.layout.image_layout);
        imagesAdapter = new ImagesAdapter(this, images, R.layout.image_layout);

        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new GridLayoutManager(this, 2);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        //setting the start up option:
        recyclerView.setAdapter(imagesAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.links_adapter:
                recyclerView.setAdapter(animalAdapter);
                return true;
            case R.id.resources_adapter:
                recyclerView.setAdapter(partyAdapter);
                return true;
            case R.id.memory_adapter:
                checkForPermission();
                images.clear();
                images.addAll(getImagesPath());
                recyclerView.setAdapter(imagesAdapter);
                imagesAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void checkForPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_READ_EXTERNAL_MEMORY);
        }
    }

    private boolean hasPermission(String permissionToCheck) {
        int permissionCheck = ContextCompat.checkSelfPermission(this, permissionToCheck);
        return (permissionCheck == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_READ_EXTERNAL_MEMORY:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    images.clear();
                    images.addAll(getImagesPath());
                    imagesAdapter.notifyDataSetChanged();
                }
                break;
            default:
                break;
        }
    }

    public String[] getAnimalsLinks() {
        String[] links = {
                "https://static.pexels.com/photos/86462/red-kite-bird-of-prey-milan-raptor-86462.jpeg",
                "https://images.pexels.com/photos/4759196/pexels-photo-4759196.jpeg",
                "https://static.pexels.com/photos/55814/leo-animal-savannah-lioness-55814.jpeg",
                "https://static.pexels.com/photos/40745/red-squirrel-rodent-nature-wildlife-40745.jpeg",
                "https://static.pexels.com/photos/33392/portrait-bird-nature-wild.jpg",
                "https://static.pexels.com/photos/62640/pexels-photo-62640.jpeg",
                "https://static.pexels.com/photos/38438/rattlesnake-toxic-snake-dangerous-38438.jpeg",
                "https://static.pexels.com/photos/33149/lemur-ring-tailed-lemur-primate-mammal.jpg",
                "https://images.pexels.com/photos/3026368/pexels-photo-3026368.jpeg",
                "https://static.pexels.com/photos/40731/ladybug-drop-of-water-rain-leaf-40731.jpeg",
                "https://static.pexels.com/photos/40860/spider-macro-insect-arachnid-40860.jpeg",
                "https://static.pexels.com/photos/63282/crab-yellow-ocypode-quadrata-atlantic-ghost-crab-63282.jpeg",
                "https://static.pexels.com/photos/45246/green-tree-python-python-tree-python-green-45246.jpeg",
                "https://static.pexels.com/photos/39245/zebra-stripes-black-and-white-zoo-39245.jpeg",
                "https://images.pexels.com/photos/4755027/pexels-photo-4755027.jpeg",
                "https://images.pexels.com/photos/4423850/pexels-photo-4423850.jpeg",
                "https://images.pexels.com/photos/2598024/pexels-photo-2598024.jpeg",
                "https://static.pexels.com/photos/52961/antelope-nature-flowers-meadow-52961.jpeg",
                "https://static.pexels.com/photos/36450/flamingo-bird-pink-nature.jpg",
                "https://static.pexels.com/photos/20861/pexels-photo.jpg",
                "https://static.pexels.com/photos/54108/peacock-bird-spring-animal-54108.jpeg",
                "https://images.pexels.com/photos/2937623/pexels-photo-2937623.jpeg"
        };
        return links;
    }

    public int[] getPartyPics() {
        int[] values = {
                R.drawable.ballons,
                R.drawable.christmas,
                R.drawable.concert,
                R.drawable.drinks,
                R.drawable.fiction,
                R.drawable.fire,
                R.drawable.glass,
                R.drawable.fireworks,
                R.drawable.glass,
                R.drawable.guy,
                R.drawable.notice,
                R.drawable.olives
        };
        return values;
    }

    public List<String> getImagesPath() {
        List<String> listOfAllImages = new ArrayList<String>();

        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};

            Cursor cursor = getContentResolver()
                            .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, MediaStore.Images.Media._ID);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                        listOfAllImages.add(path);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        }
        return listOfAllImages;
    }

    @Override
    protected void onResume() {
        super.onResume();
        images.clear();
        images.addAll(getImagesPath());
        imagesAdapter.notifyDataSetChanged();
    }
}