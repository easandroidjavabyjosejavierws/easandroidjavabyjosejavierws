package com.example.section_02e;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/*lesson 54 section 5.
    the idea is to include a button in the action bar.
 */
public class GridActivity extends AppCompatActivity {

    private GridView gridView;
    List<String> names = new ArrayList<>();
    private int counter = 0;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        gridView = findViewById(R.id.gridView);

        //data to render is added to the listView
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");

        //add listener to the listView using lambdas:
        gridView.setOnItemClickListener((adapterView, view, position, id) -> {
            Toast.makeText(GridActivity.this, names.get(position) + " was clicked.", Toast.LENGTH_LONG).show();
        });

        //works with a customized adapter.
        myAdapter = new MyAdapter(this, R.layout.grid_item, names);
        gridView.setAdapter(myAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            //a new name is added.
            case R.id.add_item:
                names.add("added #:" + (++counter));
                //the adapter must be notified about this change.
                myAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
