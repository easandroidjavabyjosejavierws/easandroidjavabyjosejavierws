package com.example.section_02e;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<String> names;

    //constructor
    public MyAdapter(Context context, int layout, List<String> names) {
        this.context    = context;
        this.layout     = layout;
        this.names      = names;
    }

    @Override
    public int getCount() { //means: How many items are in the data set represented by this Adapter.
        return this.names.size();
    }

    @Override
    public Object getItem(int position) {
        return this.names.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //view holder pattern
        ViewHolder holder;

        if (convertView == null) {
            //the coming view is inflated with the customized layout (the linear list_item).
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(this.layout, null);

            holder = new ViewHolder();
            //create a reference of the element (textView) and fill it.
            holder.nameTextView = convertView.findViewById(R.id.textView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //retrieve the item based on its position.
        String currentName = names.get(position);
        //currentName = (String) getItem(position);

        //create a reference of the element (textView) and fill it.
        holder.nameTextView.setText(currentName);

        //return the view updated with my data
        return convertView;
    }

    static class ViewHolder {
        private TextView nameTextView;
    }
}
