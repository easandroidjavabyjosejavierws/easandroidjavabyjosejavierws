package com.example.section_02d;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/*it starts with lesson 53 section 5.
    the idea is to implement a gridView.
 */
public class ListActivity extends AppCompatActivity {

    private ListView listView;
    List<String> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listView = findViewById(R.id.listView);

        //data to render is added to the listView
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");

        /*adaptor: this part uses the out of pocket adaptor.
        this is the way to render the content of the listView.
         */
        //ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);

        //link the adapter to the listView
        //listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListActivity.this, names.get(position) + " was clicked.", Toast.LENGTH_LONG).show();
            }
        });

        //works with a customized adapter.
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, names);
        listView.setAdapter(myAdapter);
    }
}