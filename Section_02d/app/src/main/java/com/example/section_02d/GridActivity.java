package com.example.section_02d;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
/*lesson 53 section 5.
implement a gridActivity (this is similar to a listView).
it's using the ViewHolder pattern.
 */
public class GridActivity extends AppCompatActivity {

    private GridView gridView;
    List<String> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        gridView = findViewById(R.id.gridView);

        //data to render is added to the listView
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");

        //add listener to the listView using lambdas:
        gridView.setOnItemClickListener((adapterView, view, position, id) -> {
            Toast.makeText(GridActivity.this, names.get(position) + " was clicked.", Toast.LENGTH_LONG).show();
        });

        //works with a customized adapter.
        MyAdapter myAdapter = new MyAdapter(this, R.layout.grid_item, names);
        gridView.setAdapter(myAdapter);
    }
}
