package com.example.mylogin.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.mylogin.R;

public class LoginActivity extends AppCompatActivity {
    private EditText editTextEmail;
    private EditText editTextPassword;
    private Switch switchRemember;
    private Button btnLogin;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        bindUI();

        prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);

        setCredentialsIfExist();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //retrieve the entered data
                String email = editTextEmail.getText().toString();
                String password = editTextPassword.getText().toString();
                if (login(email, password)) {
                    goToMain();
                    saveOnPreferences(email, password);
                }
            }
        });
    }

    private void bindUI() {
        editTextEmail = findViewById(R.id.editTextTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        switchRemember = findViewById(R.id.switchRemember);
        btnLogin = findViewById(R.id.buttonLogin);
    }

    private void setCredentialsIfExist() {
        String email = getUserMailPrefs();
        String password = getUserPassPrefs();
        if (!TextUtils.isEmpty(email) &&
            !TextUtils.isEmpty(password)) {
            editTextEmail.setText(email);
            editTextPassword.setText(password);
        }
    }
    private boolean login(String email, String password) {
        if (!isValidEmail(email)){
            Toast.makeText(this, "invalid email, retry it pls.", Toast.LENGTH_LONG).show();
            return false;
        } else if (!isValidPassword(password)) {
            Toast.makeText(this, "invalid password, retry it pls.", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    /*saving email & password in preferences if
    the user selected it using the switch.
     */
    private void saveOnPreferences(String email, String password) {
        if (switchRemember.isChecked()) {
            SharedPreferences.Editor editor = prefs.edit();
            //the putString() are executed asynchronously.
            editor.putString("email", email);
            editor.putString("pass", password);
            editor.apply();
        }
    }
    //data validations
    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) &&
                Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidPassword(String password) {
        return password.length() >= 4;
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private String getUserMailPrefs() {
        return prefs.getString("email", "");
    }

    private String getUserPassPrefs() {
        return prefs.getString("pass", "");
    }
}