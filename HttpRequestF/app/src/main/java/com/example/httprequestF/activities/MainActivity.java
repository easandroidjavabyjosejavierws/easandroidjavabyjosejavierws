package com.example.httprequestF.activities;

import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequestF.R;
import com.example.httprequestF.models.City;
import com.example.httprequestF.services.WeatherService;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
lesson 179 section 18.
working with retrofit.
basic request.
an interface declares a call to the service [get()].
implemented code to render an error msg and kill the app
if the city doesn't exist.
Also kill the app after the service is successfully done.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("https://api.openweathermap.org/data/2.5/")
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
        WeatherService service = retrofit.create(WeatherService.class);

        Call<City> cityCall = service.getCity("Málaga, CO", "45d0dd1b82ad291e225bc56bcf572db0");
        //Call<City> cityCall = service.getCity("xyz", "45d0dd1b82ad291e225bc56bcf572db0");
        //at this point the request is prepared but not executed.
        cityCall.enqueue(new Callback<City>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (response.body() == null) {
                    String error = null;
                    try {
                        error = response.errorBody().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(MainActivity.this, "Request failed" + error, Toast.LENGTH_LONG).show();
                    finishAndRemoveTask();

                } else {
                    City city = response.body();
                    finishAndRemoveTask();
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Request failed", Toast.LENGTH_LONG).show();
            }
        });
    }
}