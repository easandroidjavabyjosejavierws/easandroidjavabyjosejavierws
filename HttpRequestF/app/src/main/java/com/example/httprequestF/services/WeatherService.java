package com.example.httprequestF.services;

import com.example.httprequestF.models.City;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {
    @GET("weather")
    //http://api.openweathermap.org/data/2.5/weather?q=Seville,es&appid={your api key}
    Call<City> getCity(@Query("q") String city, @Query("appid") String key);
}
