package com.example.section_06_fragmentsc.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.section_06_fragmentsc.R;

public class DataFragment extends Fragment {
    private EditText textData;
    private Button btnSend;
    private DataListener callback;

    public DataFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            callback = (DataListener) context;
        } catch (Exception e) {
            throw new ClassCastException(context.toString() + " should implement DataListener interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);
        /* the first action when as generates the class is to create
        a view that it will replace the original return value.
        this is because with the original return is not possible to add
        code (logic) in this method.
        original return statement:
            return inflater.inflate(R.layout.fragment_data, container, false);
        new statement:
            return view.
        the view is instantiated with the same values as the original return.
         */
        textData = view.findViewById(R.id.editTextData);
        btnSend = view.findViewById(R.id.buttonSend);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.sendData(textData.getText().toString());
            }
        });

        return view;
    }

    public interface DataListener {
        void sendData(String text);
    }
}