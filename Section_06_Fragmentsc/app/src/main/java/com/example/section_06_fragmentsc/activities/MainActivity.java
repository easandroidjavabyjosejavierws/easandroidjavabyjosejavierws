package com.example.section_06_fragmentsc.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.example.section_06_fragmentsc.R;
import com.example.section_06_fragmentsc.fragments.DataFragment;
import com.example.section_06_fragmentsc.fragments.DetailsFragment;

/* lesson 107 section 9.
working with fragments.
I started with: portrait (activity_main) and landscape [activity_main (land)] layouts
for the phone.
    portrait [activity_main (xlarge)] and landscape [activity_main (xlarge-land)] layouts
for the tablet.
Then I created additional layouts only for the phone: portrait (activity_details)
and landscape [activity_details (land)].
the layouts for the tablet contain 2 fragments.
the scenario is: the phone has 2 activities (each one with one fragment).
                the table has only on activity with 2 fragments.
                one fragment gets data and passes it to the next fragment either
                in the same activity (tablet case), or to different activity (phone case).
 */
public class MainActivity extends FragmentActivity implements DataFragment.DataListener {
    private boolean isMultiPanel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //check if we are working with the phone or the tablet.
        setMultiPanel();
    }

    @Override
    public void sendData(String text) {
        if (isMultiPanel) {
            //instantiate the details fragment.
            DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.detailsFragment);
            //pass the info from data fragment to details fragment.
            detailsFragment.renderText(text);
        } else {
            Intent intent = new Intent(this, DetailsActivity.class);
            intent.putExtra("text", text);
            startActivity(intent);
        }
    }

    private void setMultiPanel() {
        isMultiPanel = (getSupportFragmentManager().findFragmentById(R.id.detailsFragment) != null);
        /* when true: means the tablet. it necessarily has the two fragments.
        when false: means the phone because the detail fragment is missing. (it is in a
        different activity).
         */
    }
}