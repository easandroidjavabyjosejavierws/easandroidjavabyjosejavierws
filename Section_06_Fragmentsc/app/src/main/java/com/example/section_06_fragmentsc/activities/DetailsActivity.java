package com.example.section_06_fragmentsc.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.section_06_fragmentsc.R;
import com.example.section_06_fragmentsc.fragments.DetailsFragment;

public class DetailsActivity extends AppCompatActivity {
    private String text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        if (getIntent().getExtras() != null) {
            text = getIntent().getStringExtra("text");
        }

        //instantiate the details fragment.
        DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.detailsFragment);
        //pass the info from data fragment to details fragment.
        detailsFragment.renderText(text);

    }
}