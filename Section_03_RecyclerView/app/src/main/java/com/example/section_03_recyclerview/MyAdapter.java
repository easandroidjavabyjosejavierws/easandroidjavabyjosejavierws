package com.example.section_03_recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
/* this class (my adapter) contains an internal interface (OnItemClickListener)
to handle all events with the elements of my layout.
also, it contains an internal class (ViewHolder) to handle the viewHolder pattern.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<String> names;
    private int layout;
    private OnItemClickListener itemClickListener;

    //constructor
    public MyAdapter(List<String> names, int layout, OnItemClickListener listener) {
        this.names          = names;
        this.layout         = layout;
        itemClickListener   = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        //instantiate the viewHolder
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(names.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;

        //constructor
        public ViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
        }

        public void bind(final String name, final OnItemClickListener listener) {
            this.textViewName.setText(name);
            itemView.setOnClickListener((v) -> {
                listener.onItemClick(name, getAdapterPosition());
            });
        }
    }

    @FunctionalInterface
    public interface OnItemClickListener {
        void onItemClick(String name, int position);
    }
}
