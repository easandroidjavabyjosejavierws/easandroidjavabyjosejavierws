package com.example.section_03_recyclerview;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /*lessons 57, 58 and 59 section 6.
    the idea is to implement a recycler view.
    */
    private List<String> names;
    private RecyclerView mRecyclerView; //an instantiation of the recyclerView
    private RecyclerView.Adapter mAdapter; //an instantiation of my adapter
    private RecyclerView.LayoutManager mLayoutManager; //my layout rendered by rView


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //fulfill the list array
        names = getAllNames();
        mRecyclerView = findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        /* the customized adapter contains all code to handle the rV.
         */

        mAdapter = new MyAdapter(names, R.layout.recycler_view_item, new MyAdapter.OnItemClickListener() {
            /* new MyAdapter.OnItemClickListener() is an instantiation of the
            OnItemClickListener internal interface defined in my adapter
             */
            @Override
            public void onItemClick(String name, int position) {
                Toast.makeText(MainActivity.this, name + " - " + position, Toast.LENGTH_LONG).show();
            }
        });

        //this is equivalent ot inflate the rV with my layout.
        mRecyclerView.setLayoutManager(mLayoutManager);
        //links my adapter with the rV.
        mRecyclerView.setAdapter(mAdapter);
    }

    private List<String> getAllNames() {
        return new ArrayList<String>() {{
            add("Orlando");
            add("Lina Maria");
            add("Diana Jimena");
            add("Nicholas");
            add("Sophia");
            add("Patrick");
        }};
    }
}
