package com.example.section_01b;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
/* lesson 35 section 4.

 */
public class MainActivity extends AppCompatActivity {

    /* this simply renders (uses Toast) a message
        working code, without using the button property.
        it uses an anonymous class (OnClickListener).
    */
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.button_Main);
        //findViewById() returns a View object (a component).
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,
                        "button clicked using code with anonymous class",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}
