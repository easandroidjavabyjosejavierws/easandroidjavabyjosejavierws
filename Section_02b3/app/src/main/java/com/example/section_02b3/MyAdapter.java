package com.example.section_02b3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
/* this class (my adapter) contains an internal interface (OnItemClickListener)
to handle all events with the elements of my layout.
also, it contains an internal class (ViewHolder) to handle the viewHolder pattern.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<String> names;
    private int layout;
    private HandlePlayButton itemClickListener;

    //constructor
    public MyAdapter(List<String> names, int layout, HandlePlayButton listener) {
        this.names          = names;
        this.layout         = layout;
        itemClickListener   = listener;
    }

    @NonNull
    @Override
    /*create a view and inflate it based on the incoming layout.
    also, it instantiates the ViewHolder object.
     */
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        //instantiate the viewHolder
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    /*this supposes to fill the inflated view with data coming from the collection
    but rather to do it in here, I am using a bind() defined into the ViewHolder class.
     */
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(names.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public Button gameButton;

        //constructor
        public ViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.gameButton = itemView.findViewById(R.id.play_game_btn);
        }

        /* fills the inflated view with data from the collection
        and sets up the listener.
         */
        public void bind(final String name, final HandlePlayButton listener) {

            this.textViewName.setText(name);

            //set up a listener for the "Play" button
            gameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(name, getAbsoluteAdapterPosition());
                }
            });
        }
    }

    //interface
    public interface HandlePlayButton {
        void onItemClicked(String name, int position);
    }
}
