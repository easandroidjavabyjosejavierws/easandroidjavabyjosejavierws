package com.example.section_02b3;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
/*lessons 60 and 61 section 6.
 */
public class MainActivity extends AppCompatActivity {

    /*
    the idea is to improve a recycler view
    with add, delete and some animation,
    plus rendering GridLayoutManager and  StaggeredGridLayoutManager too.
    */
    private List<String> names;

    private RecyclerView mRecyclerView; //an instantiation of the recyclerView
    private RecyclerView.Adapter mAdapter; //an instantiation of my adapter
    private RecyclerView.LayoutManager mLayoutManager; //my layout rendered by rView

    private int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //fulfill the list array
        names = getAllNames();
        mRecyclerView = findViewById(R.id.recyclerView);
        //this renders a linear layout
        mLayoutManager = new LinearLayoutManager(this);
        //this renders a grid layout
        //mLayoutManager = new GridLayoutManager(this, 2);
        /*this renders a StaggeredGrid layout. it's similar to GridLayoutManager
        but allows to render images with different sizes.
         */
        //mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        /* the customized adapter contains all code to handle the rV.
         */
        mAdapter = new MyAdapter(names, R.layout.recycler_view_item, new MyAdapter.HandlePlayButton() {
            /* new MyAdapter.HandlePlayButton() is an instantiation of the
            OnItemClickListener internal interface defined in my adapter
             */
            @Override
            public void onItemClicked(String name, int position) {
                Toast.makeText(MainActivity.this, name + " - " + position, Toast.LENGTH_SHORT).show();
                //deleteName(position);
            }
        });

        /*this improve the performance if you know that your layout
        won't change in size (not the list size), for example the
        characters in the name have a fixed length.
         */
        mRecyclerView.setHasFixedSize(true);
        //this implements an animator.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        //this is equivalent ot inflate the rV with my layout.
        mRecyclerView.setLayoutManager(mLayoutManager);
        //links my adapter with the rV.
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_name:
                this.addName(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private List<String> getAllNames() {
        return new ArrayList<String>() {{
            add("Orlando");
            add("Lina Maria");
            add("Diana Jimena");
            add("Nicholas");
            add("Sophia");
            add("Patrick");
        }};
    }

    private void addName(int position) {
        names.add(position, "new name " + (++counter));
        mAdapter.notifyItemInserted(position);
        mLayoutManager.scrollToPosition(position);
    }

    private void deleteName(int position) {
        names.remove(position);
        mAdapter.notifyItemRemoved(position);
    }
}
