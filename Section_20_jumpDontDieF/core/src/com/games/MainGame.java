package com.games;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

/*
lesson 224-228 Section 20.
preparing for Box2D, creating he actor.
conversion logic between meters and pixels.
Scene2D works in pixels and Box2D works in meters.
create PlayerEntity.
create FloorEntity and SpikeEntity.
render all actors: floor, player, and mr. pinchos.
 */
public class MainGame extends Game {
    private AssetManager manager;

    public AssetManager getManager() {
        return manager;
    }
    @Override
    public void create() {
        manager = new AssetManager();
        manager.load("floor.png", Texture.class);
        manager.load("overfloor.png", Texture.class);
        manager.load("spike.png", Texture.class);
        manager.load("player.png", Texture.class);
        manager.finishLoading();
        setScreen(new GameScreen(this)); //this calls the starter screen.
    }
}
