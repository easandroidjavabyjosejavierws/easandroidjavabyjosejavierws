package com.example.section_06_fragments_labb.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.section_06_fragments_labb.R;
import com.example.section_06_fragments_labb.fragments.ListFragment;
import com.example.section_06_fragments_labb.models.Mail;

/*lesson 108, section 9.
    fragment lab.
    this version implements the listView with 
    the fragments and logic only for the phone.
 */
public class MainActivity extends AppCompatActivity implements ListFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public void onListClick(Mail mail) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("subject", mail.getSubject());
        intent.putExtra("message", mail.getMessage());
        intent.putExtra("senderName", mail.getSenderName());
        startActivity(intent);
        //Toast.makeText(MainActivity.this,  " a mail was clicked.", Toast.LENGTH_LONG).show();
    }
}