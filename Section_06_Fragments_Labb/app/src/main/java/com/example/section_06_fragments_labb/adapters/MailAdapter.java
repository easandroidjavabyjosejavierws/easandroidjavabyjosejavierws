package com.example.section_06_fragments_labb.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.section_06_fragments_labb.R;
import com.example.section_06_fragments_labb.models.Mail;

import java.util.List;

public class MailAdapter  extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Mail> list;
    private int SUBJECT_MAX_LENGHT = 40;
    private int MESSAGE_MAX_LENGHT = 80;

    //constructor
    public MailAdapter(Context context, int layout, List<Mail> list) {
        this.context = context;
        this.layout = layout;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(layout, null);
            holder = new ViewHolder();
            holder.subject = convertView.findViewById(R.id.textViewListSubject);
            holder.message = convertView.findViewById(R.id.textViewListMessage);
            holder.sender = convertView.findViewById(R.id.textViewListSenderName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Mail currentMail = (Mail) getItem(position);

        /*if mail title is less than 40 chars, it stays as it's, else it
        needs to be truncated and appended periods.
         */
        String shortSubject;
        if (currentMail.getSubject().length() > SUBJECT_MAX_LENGHT) {
            shortSubject = currentMail.getSubject().substring(0, SUBJECT_MAX_LENGHT) + "...";
        } else {
            shortSubject = currentMail.getSubject();
        }
        holder.subject.setText(shortSubject);

        /* if the mail context contains less than 40 words, it stays as it's, else it
        needs to be truncated and appended periods.
         */
        String shortMessage;
        if (currentMail.getMessage().length() > MESSAGE_MAX_LENGHT) {
            shortMessage = currentMail.getMessage().substring(0, MESSAGE_MAX_LENGHT) + "...";
        } else {
            shortMessage = currentMail.getMessage();
        }
        holder.message.setText(shortMessage);

        // take only the first letter of the sender name.
        holder.sender.setText(currentMail.getSenderName().substring(0, 1));
        /* retrieve the background properties for this textView element (it's shape
        is oval according to the rounded_corner.xml file in drawable folder).
        using getBackground() and apply the color (from the mail object property).
         */
        holder.sender.getBackground().setColorFilter(Color.parseColor("#" + currentMail.getColor()), PorterDuff.Mode.SRC);

        return convertView;
    }

    static class ViewHolder {
        private TextView subject;
        private TextView message;
        private TextView sender;
    }
}
