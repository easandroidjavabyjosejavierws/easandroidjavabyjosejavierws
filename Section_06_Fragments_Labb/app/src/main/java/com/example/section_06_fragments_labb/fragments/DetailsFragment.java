package com.example.section_06_fragments_labb.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.section_06_fragments_labb.R;
import com.example.section_06_fragments_labb.models.Mail;

public class DetailsFragment extends Fragment {
    private TextView textViewSubject;
    private TextView textViewSender;
    private TextView textViewMessage;
    private LinearLayout wrapper;

    public DetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_details, container, false);

        textViewSubject = view.findViewById(R.id.textViewFragmentSubject);
        textViewSender = view.findViewById(R.id.textViewFragmentSenderName);
        textViewMessage = view.findViewById(R.id.textViewFragmentMessage);
        wrapper = view.findViewById(R.id.fragmentDetailsMailWrapper);
        return view;
    }

    public void renderMail(Mail mail) {
        wrapper.setVisibility(View.VISIBLE);
        textViewSubject.setText(mail.getSubject());
        textViewSender.setText(mail.getSenderName());
        textViewMessage.setText(mail.getMessage());
    }
}