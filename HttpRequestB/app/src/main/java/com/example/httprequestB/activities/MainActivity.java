package com.example.httprequestB.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequestB.R;
import com.example.httprequestB.models.City;
import com.google.gson.Gson;

/*
lesson 173 section 18.
using Gson object from google.

 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String json = "{\n" +
                      "id: 1,\n" +
                      "name: 'London'" +
                      "}";

        Gson gson = new Gson();
        City city1 = gson.fromJson(json, City.class);
        Toast.makeText(this, city1.getId() + " -- " + city1.getName(), Toast.LENGTH_LONG).show();
    }
}