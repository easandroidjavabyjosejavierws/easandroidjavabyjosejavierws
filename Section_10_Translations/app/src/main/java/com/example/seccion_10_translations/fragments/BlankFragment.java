package com.example.seccion_10_translations.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.seccion_10_translations.R;

public class BlankFragment extends Fragment {

    public BlankFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //getString(R.string.welcome);
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        return view;
    }
}