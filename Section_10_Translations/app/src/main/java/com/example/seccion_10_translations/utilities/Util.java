package com.example.seccion_10_translations.utilities;

import android.content.Context;

import com.example.seccion_10_translations.R;

public class Util {
    public static String getWelcome(Context context) {
        return context.getString(R.string.welcome);
    }
}
