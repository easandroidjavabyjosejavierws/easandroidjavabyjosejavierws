package com.example.seccion_10_translations.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.seccion_10_translations.R;
/*
lesson 145-146 Section 13.
An additional strings.xml file has been created to
keep spanish translations.
Code to get a string from a fragment or from a java class.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, getString(R.string.welcome), Toast.LENGTH_LONG).show();
    }
}