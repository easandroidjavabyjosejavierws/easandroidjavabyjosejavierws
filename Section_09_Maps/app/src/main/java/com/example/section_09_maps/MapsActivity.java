package com.example.section_09_maps;

import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
/*
lesson 126-129 section 12.
using google maps layout.
Initially was Sydney (the default) then changed it to my Home.
An instance of CameraPosition points to my home with better zoom, tilt,
and bearing.
constraints to limit zoom: setMinZoomPreference() and setMaxZoomPreference().
some listeners.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /* this couple of statements constraint the user
        to zoom further than 21 and below 10
         */
        mMap.setMinZoomPreference(10);
        mMap.setMaxZoomPreference(21);

        // Add a marker in my home and move the camera
        LatLng myHome = new LatLng(26.6072037, -80.1609351);
        LatLng linas = new LatLng(26.5688113, -80.151);
        LatLng sevilla = new LatLng(37.40911491941731, -5.99075691250005);
        mMap.addMarker(new MarkerOptions().position(myHome).title("Hello from my home").draggable(true));
        /* moveCamera() is a basic method.
        there is a better way to manipulate the camera creating a CameraPosition instance.
         */
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(myHome));
        CameraPosition cameraTowardsHome = new CameraPosition.Builder()
                .target(myHome)
                .zoom(20) //limit 21
                .bearing(90) //camera orientation to the east in degrees. (0 to 365).
                .tilt(45) //impacts the buildings 3D view. it's in degrees [0 to 90].
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraTowardsHome));

        //events
        //triggered when user click on the map.
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Toast.makeText(MapsActivity.this, "click on: \n" +
                        "Lat: " + latLng.latitude + "\n" +
                        " Long: " + latLng.longitude, Toast.LENGTH_LONG).show();
            }
        });

        //triggered when user takes a longer click on the map.
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                Toast.makeText(MapsActivity.this, "long click on: \n" +
                        "Lat: " + latLng.latitude + "\n" +
                        " Long: " + latLng.longitude, Toast.LENGTH_LONG).show();
            }
        });

        //triggered when user drags the marker on the map.
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Toast.makeText(MapsActivity.this, "marker dragged to: \n" +
                        "Lat: " + marker.getPosition().latitude + "\n" +
                        " Long: " + marker.getPosition().longitude, Toast.LENGTH_LONG).show();
            }
        });
    }
}