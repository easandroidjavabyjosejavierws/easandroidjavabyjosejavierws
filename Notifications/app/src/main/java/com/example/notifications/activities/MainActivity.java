package com.example.notifications.activities;

import android.app.Notification;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.example.notifications.R;
import com.example.notifications.notifications.NotificationHandler;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/*
lesson 185-190 section 19.
defining and creating two channels.
implement methods to create notifications taking care of the android version.
include the butter knife library (gradle at module:app). This requires java 1.8.
bind components with butter knife annotations.
set up listeners with butter knife annotations.
adding extra configurations.
changing the switch label and padding, setting off as default
and changing it programmatically.
rendering the notification.

 */
public class MainActivity extends AppCompatActivity {
    @BindView(R.id.editTextTitle)
    EditText editTextTitle;
    @BindView(R.id.editTextMessage)
    EditText editTextMessage;
    @BindView(R.id.switchImportance)
    Switch switchImportance;
    @BindString(R.string.switch_notifications_on) String switchTextOn;
    @BindString(R.string.switch_notifications_off) String switchTextOff;
    private boolean isHighImportance = false;
    private NotificationHandler notificationHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this); //right after setContentView.
        notificationHandler = new NotificationHandler(this);
    }

    @OnClick(R.id.buttonSend)
    public void click() {
        sendNotification();
    }

    @OnCheckedChanged(R.id.switchImportance)
    public void change(CompoundButton buttonView, boolean isChecked) {
        isHighImportance = isChecked;
        //Toast.makeText(this, "Working: " + isHighImportance, Toast.LENGTH_LONG).show();
        switchImportance.setText((isChecked) ? switchTextOn : switchTextOff);
    }
    private void sendNotification() {
        //Toast.makeText(this, "Working", Toast.LENGTH_LONG).show();
        String title = editTextTitle.getText().toString();
        String message = editTextMessage.getText().toString();

        if (!TextUtils.isEmpty(title) &&    //isEmpty() checks if title was entered.
            !TextUtils.isEmpty(message)) {
            Notification.Builder nb = notificationHandler.createNotification(title, message, isHighImportance);
            notificationHandler.getManager().notify(1, nb.build()); //this triggers the notification.
        }
    }
}