package com.example.section_06_fragments.activities;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.example.section_06_fragments.R;
import com.example.section_06_fragments.fragments.DataFragment;
import com.example.section_06_fragments.fragments.DetailsFragment;

/* lesson 101, 102, 103, 104, 105 section 9.
working with fragments.
 */
public class MainActivity extends FragmentActivity implements DataFragment.DataListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void sendData(String text) {
        //instantiate the details fragment.
        DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.detailsFragment);
        //pass the info from data fragment to details fragment.
        detailsFragment.renderText(text);
    }
}