package com.example.httprequestC.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequestC.R;
import com.example.httprequestC.models.City;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/*
lesson 174 section 18.
using Gson object from google and excluding
attributes of the model object (City).

 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String json = "{\n" +
                      "id: 1,\n" +
                      "name: 'London'" +
                      "}";

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        //exclude the id. by default renders 0.
        City city1 = gson.fromJson(json, City.class);
        Toast.makeText(this, city1.getId() + " -- " + city1.getName(), Toast.LENGTH_LONG).show();
    }
}