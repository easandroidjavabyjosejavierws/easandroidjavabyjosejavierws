package com.example.section_04_realm_intro.application;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

public class MyMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        // DynamicRealm exposes an editable schema
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            /*
            schema.create("Person")
                    .addField("id", Integer.class)
                    .addField("name", String.class)
                    .addField("dogs", schema.get("Dog"));
            oldVersion++;
            */

        }

        if (oldVersion == 1) {
            schema.get("Person")
                    .removeField("id")
                    .addField("id", Integer.class, FieldAttribute.PRIMARY_KEY);
            oldVersion++;
        }
    }
}
