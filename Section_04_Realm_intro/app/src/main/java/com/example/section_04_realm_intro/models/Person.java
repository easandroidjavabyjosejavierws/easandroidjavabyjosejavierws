package com.example.section_04_realm_intro.models;

import com.example.section_04_realm_intro.application.MyApplication;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Person extends RealmObject {
    @PrimaryKey
    private int Id;
    private String Name;
    private RealmList<Dog> Dogs;

    //constructors
    public Person() {//this is a realm requirement.
    }

    public Person(String name) {
        Id = MyApplication.PersonID.incrementAndGet();
        Name = name;
        Dogs = new RealmList<Dog>();
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getId() {
        return Id;
    }

    public RealmList<Dog> getDogs() {
        return Dogs;
    }
}
