package com.example.section_04_realm_intro.models;

import com.example.section_04_realm_intro.application.MyApplication;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Dog  extends RealmObject {
    @PrimaryKey
    private int Id;
    private String Name;

    //constructors
    public Dog() { //this is a realm requirement.
    }

    public Dog(String name) {
        Id = MyApplication.DogID.incrementAndGet();
        Name = name;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
