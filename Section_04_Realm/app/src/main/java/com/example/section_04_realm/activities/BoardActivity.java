package com.example.section_04_realm.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.section_04_realm.R;
import com.example.section_04_realm.adapters.BoardAdapter;
import com.example.section_04_realm.models.Board;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/*lesson 75 through 89 section 7.
this is the main activity.
this project works with a listView and implements the ViewHolder pattern.
it doesn't contain the complexity of the RecyclerView.
     */
public class BoardActivity extends AppCompatActivity implements RealmChangeListener<RealmResults<Board>>,
        AdapterView.OnItemClickListener {
    private Realm realm;
    private FloatingActionButton fab;
    private ListView listView;
    private BoardAdapter adapter;
    private RealmResults<Board> boards;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);

        //realm db
        realm = Realm.getDefaultInstance();
        boards = realm.where(Board.class).findAll();
        /* this implements code for one listener.
        if I need more I have to create a switch in addChangeListener()
         to select between the different events.
         basically it detects changes with the board objects.
         */
        boards.addChangeListener(this);

        adapter = new BoardAdapter(this, boards, R.layout.list_view_board_item);
        listView = findViewById(R.id.listViewBoard);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        fab= findViewById(R.id.fabAddBoard);
        /*this links with an instance of the View.OnClickListener().
        where the onClick(View v) is executed.
         */
        fab.setOnClickListener((v) -> {
            showAlertForCreatingBoard("add a new board", "type a name for your new board");
        });
/*        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertForCreatingBoard("add a new board", "type a name for your new board");
            }
        });*/
        //showAlertForCreatingBoard("testing title", "testing message");
        /* the next three statements are temporary.
        they delete info from db.
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
         */

        registerForContextMenu(listView);
    }


    //crud actions
    private void createNewBoard(String boardName) {
        /* to work with real db I need to follow a protocol:
        I must create a transaction to have commit and rollback.
        there are two ways:
        a. with a begin and end
            realm.beginTransaction();
            ......
            realm.commitTransaction();
        b. using an anonymous class: //this is preferred when big actions
            realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
            ......
            }
         */
        realm.beginTransaction();
        Board board = new Board(boardName);
        realm.copyToRealm(board);
        realm.commitTransaction();
    }

    private void editBoard(String newName, Board board) {
        realm.beginTransaction();
        board.setTitle(newName);
        realm.copyToRealmOrUpdate(board);
        realm.commitTransaction();
    }
    private void deleteBoard(Board board) {
        realm.beginTransaction();
        board.deleteFromRealm();
        realm.commitTransaction();
    }

    private void deleteAll() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    //dialogs
    private void showAlertForCreatingBoard(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (title != null) {
            builder.setTitle(title);
        }
        if (message != null) {
            builder.setMessage(message);
        }
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_create_board, null);
        builder.setView(viewInflated);

        final EditText input = viewInflated.findViewById(R.id.editTextNewBoard);

        builder.setPositiveButton("you must Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String boardName = input.getText().toString().trim();
                if (boardName.length() > 0) {
                    createNewBoard(boardName);
                } else {
                    Toast.makeText(BoardActivity.this, "A name is required to create a new board", Toast.LENGTH_LONG).show();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAlertForEditingBoard(String title, String message, final Board board) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (title != null) {
            builder.setTitle(title);
        }
        if (message != null) {
            builder.setMessage(message);
        }
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_create_board, null);
        builder.setView(viewInflated);

        final EditText input = viewInflated.findViewById(R.id.editTextNewBoard);
        input.setText(board.getTitle());
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String boardName = input.getText().toString().trim();
                if (boardName.length() == 0) {
                    Toast.makeText(BoardActivity.this, "The name is required to edit the current board", Toast.LENGTH_LONG).show();
                } else if (boardName.equals(board.getTitle())){
                    Toast.makeText(BoardActivity.this, "The name is the same as it was before", Toast.LENGTH_LONG).show();
                } else {
                     editBoard(boardName, board);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*events */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_board_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.delete_all:
                deleteAll(); //delete all tables from db????
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(boards.get(info.position).getTitle());
        getMenuInflater().inflate(R.menu.context_menu_board_activity, menu);
        //super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete_board: //deletes only one note
                deleteBoard(boards.get(info.position));
                return true;
            case R.id.edit_board:
                showAlertForEditingBoard("edit board", "change the name of the board", boards.get(info.position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onChange(RealmResults<Board> boards) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(BoardActivity.this, NoteActivity.class);
        intent.putExtra("id", boards.get(position).getId());
        startActivity(intent);
    }

/*    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }*/

/*    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }*/
}