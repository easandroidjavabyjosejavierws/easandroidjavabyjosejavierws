package com.example.section_04_realm.models;

import com.example.section_04_realm.application.MyApplication;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Board extends RealmObject {
    @PrimaryKey //the required is implicit for id.
    private int id;
    @Required //this applies to title.
    private String title;
    @Required
    private Date createdAt;

    /*this creates a relation 1 to 0..many (one board to 0 or
    many notes.)
     */
    private RealmList<Note> notes;

    //constructors
    public Board() {
    }

    public Board(String title) {
        this.id = MyApplication.BoardID.incrementAndGet();
        this.title = title;
        this.createdAt = new Date();
        this.notes = new RealmList<Note>();
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public RealmList<Note> getNotes() {
        return notes;
    }
}
