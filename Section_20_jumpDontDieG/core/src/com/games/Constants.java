package com.games;

public class Constants {
    public static final float PIXELS_IN_METER = 45f; //original was 90f
    public static final int IMPULSE_JUMP = 20;
    public static final float PLAYER_SPEED = 8f;
}
