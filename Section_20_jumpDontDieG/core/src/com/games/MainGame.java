package com.games;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;

/*
lesson 229-234 Section 20.
adding new floor.
camera follows the player.
adding more floors and spikes.
adding music.
new screen to display "game over" and it allows to start
a new game.
Activate button from the gameOverScreen to start the game.
Allow to restart the game when player dies. The control
waits for 1.5 seconds and then goes to the gameOverScreen.
add a loading screen showing the progress and then starts
the game.
 */
public class MainGame extends Game {
    public Screen creditsScreen;
    private AssetManager manager;
    public GameScreen gameScreen;
    public GameOverScreen gameOverScreen;
    public MenuScreen menuScreen;
    public LoadingScreen loadingScreen;

    public AssetManager getManager() {
        return manager;
    }
    @Override
    public void create() {
        manager = new AssetManager();
        manager.load("floor.png", Texture.class);
        manager.load("gameover.png", Texture.class);
        manager.load("overfloor.png", Texture.class);
        manager.load("logo.png", Texture.class);
        manager.load("spike.png", Texture.class);
        manager.load("player.png", Texture.class);
        manager.load("die.ogg", Sound.class);
        manager.load("jump.ogg", Sound.class);
        manager.load("song.ogg", Sound.class);
        //manager.finishLoading();

        loadingScreen = new LoadingScreen(this);
        setScreen(loadingScreen);
    }

    public  void finishLoading() {
        menuScreen = new MenuScreen(this);
        gameScreen = new GameScreen(this);
        gameOverScreen = new GameOverScreen(this);
        setScreen(menuScreen);
    }
}
