package com.example.section_06_fragments_labC.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.section_06_fragments_labC.R;
import com.example.section_06_fragments_labC.fragments.DetailsFragment;
import com.example.section_06_fragments_labC.models.Mail;

public class DetailsActivity extends AppCompatActivity {
    private String subject;
    private String message;
    private String sender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        if (getIntent().getExtras() != null) {
            subject = getIntent().getStringExtra("subject");
            message = getIntent().getStringExtra("message");
            sender = getIntent().getStringExtra("senderName");
        }

        Mail mail = new Mail(subject, message, sender);

        DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentDetailsMail);
        detailsFragment.renderMail(mail);
    }
}