package com.example.section_06_fragments_labC.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.section_06_fragments_labC.R;
import com.example.section_06_fragments_labC.fragments.DetailsFragment;
import com.example.section_06_fragments_labC.fragments.ListFragment;
import com.example.section_06_fragments_labC.models.Mail;

/*lesson 108, section 9.
    fragment lab.
    this version implements the listView with all of
    ui requirements: landscape layout for the phone
    and portrait and landscape layout for a tablet.
 */
public class MainActivity extends AppCompatActivity implements ListFragment.OnFragmentInteractionListener {
    private boolean isMultiPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setMultiPanel();
    }


    @Override
    public void onListClick(Mail mail) {
        if (isMultiPanel) {
            DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentDetailsMail);
            detailsFragment.renderMail(mail);
        } else {
            Intent intent = new Intent(this, DetailsActivity.class);
            intent.putExtra("subject", mail.getSubject());
            intent.putExtra("message", mail.getMessage());
            intent.putExtra("senderName", mail.getSenderName());
            startActivity(intent);
            //Toast.makeText(MainActivity.this,  " a mail was clicked.", Toast.LENGTH_LONG).show();
        }
    }

    private void setMultiPanel() {
        isMultiPanel = (getSupportFragmentManager().findFragmentById(R.id.fragmentDetailsMail) != null);
    }
}