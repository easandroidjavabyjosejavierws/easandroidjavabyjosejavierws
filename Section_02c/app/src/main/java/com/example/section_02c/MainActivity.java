package com.example.section_02c;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/*lesson 52 section 5.
    the idea is to improve the performance
    implementing a View Holder pattern.
 */
public class MainActivity extends AppCompatActivity {

    private ListView listView;
    List<String> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        //data to render is added to the listView
        names.add("orlando 0");
        names.add("lina 1");
        names.add("diana 2");
        names.add("niko 3");
        names.add("orlando 4");
        names.add("lina 5");
        names.add("diana 6");
        names.add("niko 7");
        names.add("orlando 8");
        names.add("lina 9");
        names.add("diana 10");
        names.add("niko 11");
        names.add("orlando 12");
        names.add("lina 13");
        names.add("diana 14");
        names.add("niko 15");

        //add listener to the listView using lambdas:
        listView.setOnItemClickListener((adapterView, view, position, id) -> {
            Toast.makeText(MainActivity.this, names.get(position) + " was clicked.", Toast.LENGTH_LONG).show();
        });

        //works with a customized adapter.
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, names);
        listView.setAdapter(myAdapter);
    }
}