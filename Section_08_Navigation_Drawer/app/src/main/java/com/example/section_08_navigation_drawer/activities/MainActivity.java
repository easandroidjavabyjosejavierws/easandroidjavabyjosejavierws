package com.example.section_08_navigation_drawer.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.section_08_navigation_drawer.R;
import com.example.section_08_navigation_drawer.fragments.AlertsFragment;
import com.example.section_08_navigation_drawer.fragments.EmailFragment;
import com.example.section_08_navigation_drawer.fragments.InfoFragment;
import com.google.android.material.navigation.NavigationView;

/*
lesson 117-122 section 11.
DrawerLayout, NavigationView. Just basic layouts.
hamburger icon. Opening the navigation view via the hamburger icon.
create fragments to handle the main option.
implement a listener and handler for the option selection events.
fix the rendering of the fragments (the orientation (vertical) was missing
inside of the linear layout in main activity layout.
set a default option to render when the app opens.
 */
public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbar();

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navView);

        //render a default fragment when the app opens.
        setFragmentByDefault();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                boolean fragmentTransaction = false;
                Fragment fragment = null;

                switch (item.getItemId()) {
                    case R.id.menu_email:
                        fragment = new EmailFragment();
                        fragmentTransaction = true;
                        break;
                    case R.id.menu_alert:
                        fragment = new AlertsFragment();
                        fragmentTransaction = true;
                        break;
                    case R.id.menu_info:
                        fragment = new InfoFragment();
                        fragmentTransaction = true;
                        break;
                    case R.id.menu_option_1:
                        Toast.makeText(MainActivity.this, "option 1 was clicked", Toast.LENGTH_SHORT).show();
                        break;
                }

                if (fragmentTransaction) { //this is only for the main options.
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_frame, fragment)
                            .commit();
                    item.setChecked(true); //highlights the selected option
                    getSupportActionBar().setTitle(item.getTitle()); /*sets a title on
                                                                       the action bar*/
                    drawerLayout.closeDrawers();
                }
                return true;
            }
        });
    }

    private void setToolbar() {
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        //inserting and rendering the hamburger icon.
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /*
    this method sets the first option as default when
    the app opens.
     */
    private void setFragmentByDefault() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new EmailFragment())
                .commit();
        MenuItem item = navigationView.getMenu().getItem(0);
        item.setChecked(true);
        getSupportActionBar().setTitle(item.getTitle());
    }
    /*to activate the navigation view using the
    hamburger icon.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //open the navigation view (sidebar equivalent).
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}