package com.games;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/*
lesson 198-202 Section 20.
 */
public class MainGame extends ApplicationAdapter {
    private Texture minijoe; //Texture is synonymous of image.
    private Texture pinchos;
    private TextureRegion regionPinchos; //to crop an image.
    private SpriteBatch batch; //this class allows to render images.
    private int width;
    private int height;
    private int widthJoe;
    private int heightJoe;


    @Override
    public void create() {
        minijoe = new Texture("player.png");
        pinchos = new Texture("spike.png");
        batch = new SpriteBatch();

        regionPinchos = new TextureRegion(pinchos, 0, 64, 128, 64);

        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();

        widthJoe = minijoe.getWidth();
        heightJoe = minijoe.getHeight();
    }

    @Override
    public void dispose() { //this one releases resources from the graphic adapter.
        minijoe.dispose();
        batch.dispose();
        pinchos.dispose();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 0, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //cleans the buffer_bit of the graphics adapter.
        batch.begin();
        //insert anything to render
        batch.draw(minijoe, 50, 0);
        batch.draw(pinchos, 250, 0 );

        /*batch.draw(minijoe, width - widthJoe, 0);
        batch.draw(minijoe, 0, height - heightJoe);
        batch.draw(minijoe, width - widthJoe, height - heightJoe);
        batch.draw(minijoe, 100, 100, 300, 250);*/
        batch.end();
    }
}
