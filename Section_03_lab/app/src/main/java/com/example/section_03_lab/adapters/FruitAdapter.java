package com.example.section_03_lab.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.example.section_03_lab.R;
import com.example.section_03_lab.models.Fruit;
import com.squareup.picasso.Picasso;
import java.util.List;

public class FruitAdapter extends RecyclerView.Adapter<FruitAdapter.ViewHolder> {
    private List<Fruit> fruits;
    private int layout;
    private Activity activity;
    private OnItemClickListener listener;

    /*constructor
    The activity is used rather than the context, because it's
    necessary in order to inflate the context menu.
     */

    public FruitAdapter(List<Fruit> fruits, int layout, Activity activity, OnItemClickListener listener) {
        this.fruits     = fruits;
        this.layout     = layout;
        this.activity   = activity;
        this.listener   = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v          = LayoutInflater.from(activity).inflate(layout, parent, false);
        ViewHolder vh   = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull FruitAdapter.ViewHolder holder, int position) {
        holder.bind(fruits.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return fruits.size();
    }

    /* this is the ViewHolder implementation of OnCreateContextMenuListener()
     and OnMenuItemClickListener() to use the context menu in the RecyclerView.
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,
                                                                        MenuItem.OnMenuItemClickListener{
        public TextView textViewName;
        public TextView textViewDescription;
        public TextView textViewQuantity;
        public ImageView imageViewBackground;

        //constructor
        public ViewHolder(View itemView) {
            super(itemView);
            textViewName        = itemView.findViewById(R.id.textViewName);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            textViewQuantity    = itemView.findViewById(R.id.textViewQuantity);
            imageViewBackground =  itemView.findViewById(R.id.imageViewBackground);
            /*the listener for the context menu is added rather than do it inside
             of the activity working with registerForContextMenu().
             */
            itemView.setOnCreateContextMenuListener(this);
        }

        public void bind(final Fruit fruit, final OnItemClickListener listener) {
            textViewName.setText(fruit.getName());
            textViewDescription.setText(fruit.getDescription());
            textViewQuantity.setText(fruit.getQuantity() + "");
            /*this logic constraints the quantity of elements
            per each fruit.
             */
            if (fruit.getQuantity() == Fruit.LIMIT_QUANTITY) {
                textViewQuantity.setTextColor(ContextCompat.getColor(activity, R.color.colorAlert));
                textViewQuantity.setTypeface(null, Typeface.BOLD);
            } else {
                textViewQuantity.setTextColor(ContextCompat.getColor(activity, R.color.defaultTextColor));
                textViewQuantity.setTypeface(null, Typeface.NORMAL);
            }
            //work with picasso to load the image.
            Picasso.get().load(fruit.getImgBackground()).fit().into(imageViewBackground);
            //add the click listener for each fruit element.
            imageViewBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(fruit, getAdapterPosition());
                }
            });
        }
        /*I override this method in here instead of doing inside of
        the activity
         */
        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            //retrieve the position working with getAdapterPosition().
            Fruit fruitSelected = fruits.get(this.getAdapterPosition());
            //update the properties of the icon and its title for each element.
            contextMenu.setHeaderTitle(fruitSelected.getName());
            contextMenu.setHeaderIcon(fruitSelected.getImgIcon());
            //inflate the menu.
            MenuInflater inflater = activity.getMenuInflater();
            inflater.inflate(R.menu.context_menu_fruit, contextMenu);
            //add the onMenuItemClick() to each element.
            for (int i = 0; i < contextMenu.size(); i++) {
                contextMenu.getItem(i).setOnMenuItemClickListener(this);
            }
        }

        //this is for the change event in the context.
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            // the object position is retrieved with getAdapterPosition().
            switch (menuItem.getItemId()) {
                case R.id.delete_fruit:
                    /* the notifyItemRemoved() and notifyItemChanged()
                    are part of the adapter class.
                     */
                    fruits.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    return true;
                case R.id.reset_fruit_quantity:
                    fruits.get(getAdapterPosition()).resetQuantity();
                    notifyItemChanged(getAdapterPosition());
                    return true;
                default:
                    return false;
            }
        }
    }

    //this is for the change event in the action bar.
    public interface OnItemClickListener {
        void onItemClick(Fruit fruit, int position);
    }
}
