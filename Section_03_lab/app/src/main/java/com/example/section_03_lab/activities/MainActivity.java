package com.example.section_03_lab.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.section_03_lab.R;
import com.example.section_03_lab.adapters.FruitAdapter;
import com.example.section_03_lab.models.Fruit;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    /* this project covers lesson 70 (lab) of section 6.
    */
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FruitAdapter adapter;

    private List<Fruit> fruits;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set the logo for this activity.
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fruits = this.getAllFruits();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this); //it passes the current activity object (MainActivity).
        //instantiate the customized adapter.
        adapter = new FruitAdapter(fruits, R.layout.recycler_view_fruit_item, this, new FruitAdapter.OnItemClickListener() {
            @Override
            /*this events happens when I click any fruit image.
            If the limit is < 10 it increases the total by 1.
            if the limit is = 10 it doesn't increase it and the color of the
            total is set to red.
             */
            public void onItemClick(Fruit fruit, int position) {
                fruit.addQuantity(1);
                adapter.notifyItemChanged(position);
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_fruit:
                /* Retrieve the size of the array list
                in order to know where I can insert a new one.
                 */
                int position = fruits.size();
                fruits.add(position, new Fruit("Plum " + (++counter), "Fruit added by the user", R.drawable.plum_bg, R.mipmap.ic_plum, 0));
                adapter.notifyItemInserted(position);
                layoutManager.scrollToPosition(position);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private List<Fruit> getAllFruits() {
        return new ArrayList<Fruit>() {{
            add(new Fruit("strawberry", "strawberry description", R.drawable.strawberry_bg, R.mipmap.ic_strawberry,0));
            add(new Fruit("orange", "orange description", R.drawable.orange_bg, R.mipmap.ic_orange,0));
            add(new Fruit("apple", "apple description", R.drawable.apple_bg, R.mipmap.ic_apple,0));
            add(new Fruit("banana", "banana description", R.drawable.banana_bg, R.mipmap.ic_banana,0));
            add(new Fruit("cherry", "cherry description", R.drawable.cherry_bg, R.mipmap.ic_cherry,0));
            add(new Fruit("pear", "pear description", R.drawable.pear_bg, R.mipmap.ic_pear,0));
            add(new Fruit("raspberry", "raspberry description", R.drawable.raspberry_bg, R.mipmap.ic_raspberry,0));
        }};
    }
}
