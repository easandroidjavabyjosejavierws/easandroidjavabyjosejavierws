package com.example.section_04_sqlite.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.section_04_sqlite.R;
import com.example.section_04_sqlite.SQLiteHelpers.CarsSQLiteHelper;
import com.example.section_04_sqlite.adapters.MyAdapter;
import com.example.section_04_sqlite.models.Car;

import java.util.ArrayList;
import java.util.List;

/* this project covers lesson 72 of section 7.
it builds a table working with SQLite.
 */
public class MainActivity extends AppCompatActivity {
    private Button btnCreate;
    private Button btnDelete;

    private CarsSQLiteHelper carsHelper;
    private SQLiteDatabase db;

    private ListView listView;
    private MyAdapter adapter;

    private List<Car> cars;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView    = findViewById(R.id.listView);
        //the list is created empty.
        cars        = new ArrayList<Car>();

        btnCreate = findViewById(R.id.buttonCreate);
        btnDelete = findViewById(R.id.buttonDelete);

        /* since there are two buttons (create, and delete)
        I have to define two listener. This is not the optimal
        but ....
         */
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear the table
                create();
                update();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear the table
                removeAll();
                update();
            }
        });

        //the db is opened in i/o mode.
        carsHelper = new CarsSQLiteHelper(this, "DBTest1", null, 1);
        db = carsHelper.getWritableDatabase();

        adapter = new MyAdapter(this, cars, R.layout.itemdb);
        listView.setAdapter(adapter);

        update();
    }

    private List<Car> getAllCars() {
        // all records fromm the caf table are selected.
        Cursor cursor = db.rawQuery("select * from Cars", null);
        List<Car> list = new ArrayList<Car>();

        if (cursor.moveToFirst()) {
            /*the cursor with the results is iterated.
            all data is moved to the array .
             */

            while (cursor.isAfterLast() == false) {

                int VIN = cursor.getInt(cursor.getColumnIndex("VIN"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String color = cursor.getString(cursor.getColumnIndex("color"));

                list.add(new Car(VIN, name, color));
                cursor.moveToNext();
            }
        }
        return list;
    }

    private void create() {
        /*Come here only if the db open was successful.
        it inserts a record into the cars table.
         */
        if (db != null) {
            //A new record as a ContentValues object is prepared to be inserted.
            ContentValues nuevoRegistro = new ContentValues();
            /* according the model, the ID field is auto-incr
            */
            nuevoRegistro.put("name", "Seat");
            nuevoRegistro.put("color", "Black");

            //the record is inserted into the db.
            db.insert("Cars", null, nuevoRegistro);
        }
    }

    private void removeAll() {
        db.delete("Cars", "", null);
    }

    private void update() {
        // all elements are deleted from the arrayList.
        cars.clear();
        // all elements are loaded
        cars.addAll(getAllCars());
        // the adaptor is notified of these changes.
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        /*this method is executed as part of the app life cycle.
        it closes the active connection with the db.
         */
        db.close();
        super.onDestroy();
    }
}
