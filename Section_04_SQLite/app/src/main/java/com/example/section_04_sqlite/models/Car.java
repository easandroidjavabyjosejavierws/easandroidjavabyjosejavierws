package com.example.section_04_sqlite.models;

public class Car {
    private int VIN;
    private String name;
    private String color;

    //constructor
    public Car(int VIN, String name, String color) {
        this.VIN = VIN;
        this.name = name;
        this.color = color;
    }

    public int getVIN() {
        return VIN;
    }

    public void setVIN(int VIN) {
        this.VIN = VIN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
