package com.example.section_04_sqlite.SQLiteHelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/* this class basically create the table into the db
when it's required.
 */
public class CarsSQLiteHelper extends SQLiteOpenHelper {
    // SQL query to create cars table.
    String sqlCreate = "CREATE TABLE Cars (VIN INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, color TEXT)";

    //constructor
    public CarsSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Current table is deleted.
        db.execSQL("DROP TABLE IF EXISTS Cars");

        //a new table is recreated.
        db.execSQL(sqlCreate);
    }
}
