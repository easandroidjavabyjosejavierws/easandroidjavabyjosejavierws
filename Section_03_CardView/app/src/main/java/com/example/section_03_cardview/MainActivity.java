package com.example.section_03_cardview;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
/*lessons 62 and 63 of section 6.
 */
public class MainActivity extends AppCompatActivity {
    /*
    it implements a cardView widget.
    the main layout is a linear one.
    in lesson 63 a ripple effect is added.
     */
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textViewTitle);
        textView.setText("hello from the card view :)");
    }
}
