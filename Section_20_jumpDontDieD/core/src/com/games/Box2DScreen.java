package com.games;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Box2DScreen extends BaseScreen {
    private World world;
    private Box2DDebugRenderer renderer;
    private OrthographicCamera camera;
    private Body minijoeBody;
    private Body sueloBody;
    private Fixture minijoeFixture;
    private Fixture sueloFixture;

    public Box2DScreen(MainGame game) {
        super(game);
    }

    @Override
    public void show() {
        world = new World(new Vector2(0, -10), true);
        renderer = new Box2DDebugRenderer();
        //camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera = new OrthographicCamera(7.11f, 4);
        camera.translate(2, 1);

        minijoeBody = world.createBody(createJoeBodyDef());
        sueloBody = world.createBody(createSueloBody());

        PolygonShape minijoeShape = new PolygonShape();
        minijoeShape.setAsBox(0.5f, 0.5f);
        minijoeFixture = minijoeBody.createFixture(minijoeShape, 1);
        minijoeShape.dispose();

        PolygonShape sueloShape = new PolygonShape();
        sueloShape.setAsBox(500, 1);
        sueloFixture = sueloBody.createFixture(sueloShape, 1);
        sueloShape.dispose();
    }

    private BodyDef createSueloBody() {
        BodyDef def = new BodyDef();
        def.position.set(0, -1);
        //def.type = BodyDef.BodyType.StaticBody;
        return def;
    }

    private BodyDef createJoeBodyDef() {
        BodyDef def = new BodyDef();
        def.position.set(0, 10);
        def.type = BodyDef.BodyType.DynamicBody;
        return def;
    }

    @Override
    public void dispose() {
        minijoeBody.destroyFixture(minijoeFixture);
        world.destroyBody(minijoeBody);
        world.dispose();
        renderer.dispose();
    }

    @Override
    public void render(float delta) { //this methods runs 60 times per second.
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        world.step(delta, 6, 2);

        camera.update();
        renderer.render(world, camera.combined);
    }
}
