package com.games;

import com.badlogic.gdx.Game;

/*
lesson 213-218 Section 20.
Call the new screen (Box2DScreen).
create a world and a camera.
create bodies (objects representing entities from our world)
create fixtures (body forms).
fixing the screen dimension.
create ground and tuning the camera.
 */
public class MainGame extends Game {

    @Override
    public void create() {
        setScreen(new Box2DScreen(this)); //this calls the starter screen.
    }
}
