package com.games.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
/* this class specifies the actor role.

 */
public class ActorJugador extends Actor {
    private Texture jugador;
    private boolean alive;

    public ActorJugador(Texture jugador) { //this constructor loads the jugador image.
        this.jugador = jugador;
        this.alive = true;
        setSize(jugador.getWidth(), jugador.getHeight());
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public void act(float delta) {

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(jugador, getX(), getY());
    }
}
