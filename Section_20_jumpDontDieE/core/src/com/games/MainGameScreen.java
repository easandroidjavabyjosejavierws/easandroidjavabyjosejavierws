package com.games;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.games.actors.ActorJugador;
import com.games.actors.ActorPinchos;

public class MainGameScreen extends BaseScreen {
    private Stage stage;
    private ActorJugador jugador;
    private ActorPinchos pinchos;
    private Texture textureJugador, texturePinchos;
    private TextureRegion regionPinchos;

    //constructor
    public MainGameScreen(MainGame game) {
        super(game);
        textureJugador = new Texture("player.png");
        texturePinchos = new Texture("spike.png");
        regionPinchos = new TextureRegion(texturePinchos, 0, 64, 80, 100);
    }

    @Override
    public void show() {
        stage = new Stage();
        stage.setDebugAll(true);
        jugador = new ActorJugador(textureJugador);
        pinchos = new ActorPinchos(regionPinchos);
        stage.addActor(jugador);
        stage.addActor(pinchos);

        jugador.setPosition(20f, 100f);
        pinchos.setPosition(500f, 100f);
        //pinchos.setHeight(500f);
        //pinchos.setWidth(500f);
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.4f, 0.5f, 0.8f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(); //make updates on actors (i.e. movements).
        
        comprobarColisiones();
        
        stage.draw(); //renders actors and scenario.
    }

    private void comprobarColisiones() {
        if (jugador.isAlive() &&
           (jugador.getX() + jugador.getWidth() < pinchos.getX())) {
            System.out.println("Colision");
            jugador.setAlive(false);
        }
    }

    @Override
    public void dispose() {
        textureJugador.dispose();
    }
}
