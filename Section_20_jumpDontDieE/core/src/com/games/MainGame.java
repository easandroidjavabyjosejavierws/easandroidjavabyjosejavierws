package com.games;

import com.badlogic.gdx.Game;

/*
lesson 219-223 Section 20.
custom fixtures. My new mr pinchos.
implement a basic jump (the player jumps without control, just touching the screen).
implement that minijoe jumps after a click.
implement that minijoe jumps only when you click and hold it.
fix the camera to see the collision between minijoe and mr pinchos.
Update the minijoe speed in the x-axis.
 */
public class MainGame extends Game {

    @Override
    public void create() {
        setScreen(new Box2DScreen(this)); //this calls the starter screen.
    }
}
