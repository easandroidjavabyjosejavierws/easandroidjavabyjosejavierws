package com.example.section_03_recycler_card_view;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
/*lessons 64, 65, 66 section 6.
 */
public class MainActivity extends AppCompatActivity {

    /*
    it implements a cardView widget with recycler view.
    it was copied from section_03_recyclerviewb.
    */

    private List<Movie> movies;

    private RecyclerView mRecyclerView; //an instantiation of the recyclerView
    private RecyclerView.Adapter mAdapter; //an instantiation of my adapter
    private RecyclerView.LayoutManager mLayoutManager; //my layout rendered by rView

    private int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //fulfill the list array
        movies = getAllMovies();
        mRecyclerView = findViewById(R.id.recyclerView);
        //this renders a linear layout
        mLayoutManager = new LinearLayoutManager(this);
        //this renders a grid layout
        //mLayoutManager = new GridLayoutManager(this, 2);
        /*this renders a StaggeredGrid layout. it's similar to GridLayoutManager
        but allows to render images with different sizes.
         */
        //mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        /* the customized adapter contains all code to handle the rV.
         */
        mAdapter = new MyAdapter(movies, R.layout.recycler_view_item, new MyAdapter.OnItemClickListener() {
            /* new MyAdapter.OnItemClickListener() is an instantiation of the
            OnItemClickListener internal interface defined in my adapter
             */
            @Override
            public void onItemClick(Movie movie, int position) {
                //Toast.makeText(MainActivity.this, name + " - " + position, Toast.LENGTH_LONG).show();
                //deleteName(position);
            }
        });

        /*this improve the performance if you know that your layout
        won't change in size (not the list size), for example the
        characters in the name have a fixed length.
         */
        mRecyclerView.setHasFixedSize(true);
        //this implements an animator.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        //this is equivalent ot inflate the rV with my layout.
        mRecyclerView.setLayoutManager(mLayoutManager);
        //links my adapter with the rV.
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_name:
                //this.addName(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private List<Movie> getAllMovies() {
        return new ArrayList<Movie>() {{
            add(new Movie("ben hur", R.drawable.benhur));
            add(new Movie("deadpool", R.drawable.deadpool));
            add(new Movie("guardians of the galaxy", R.drawable.guardians));
            add(new Movie("warcraft", R.drawable.warcraft));
        }};
    }
}
