package com.example.section_08_navigation_drawer_lab.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.section_08_navigation_drawer_lab.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EmailFragment extends Fragment implements View.OnClickListener, DialogInterface.OnClickListener {
    private FloatingActionButton fab;
    private TextView textViewTitle;
    private final String ORIGINALTEXTVIEWTITLE = "Your Email";
    private AlertDialog.Builder builder;
    private EditText editTextMail;

    public EmailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_email, container, false);

        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(this);

        textViewTitle = view.findViewById(R.id.textViewTitle);
        return view;
    }

    @Override
    public void onClick(View v) {
        builder = new AlertDialog.Builder(getContext());
        builder.setTitle("EMAIL");
        builder.setMessage("Type your email address to be displayed in the middle of the screen");

        // Set up the input
        editTextMail = new EditText(getContext());
        editTextMail.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setView(editTextMail);

        // Set up the buttons
        builder.setPositiveButton("OK", this);
        builder.setNegativeButton("Cancel", this);
        //textViewTitle.setText(null);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {

            String email = editTextMail.getText().toString();
            if (!email.isEmpty()) {
                textViewTitle.setText(email);
            } else {
                textViewTitle.setText(ORIGINALTEXTVIEWTITLE);
            }

        } else if (which == DialogInterface.BUTTON_NEGATIVE) {
            dialog.cancel();
        }
    }
}