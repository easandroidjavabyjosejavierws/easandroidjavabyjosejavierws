package com.example.section_08_navigation_drawer_lab.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.section_08_navigation_drawer_lab.R;
import com.example.section_08_navigation_drawer_lab.fragments.AlertsFragment;
import com.example.section_08_navigation_drawer_lab.fragments.EmailFragment;
import com.example.section_08_navigation_drawer_lab.fragments.InfoFragment;
import com.google.android.material.navigation.NavigationView;

/*
lesson 124 section 11.
lab.
the fragments render something.
the info fragment just renders a text (textView) and has a button (EXIT).
the alert fragment has a switch and 2 buttons (CANCEL and OK). Cancel returns
to the initial rendering. OK also returns to the initial rendering but with a
variable message depending to the switch status.
the email fragment has a similar behavior like the alert fragment, but rather
than a switch it has an editText and 2 buttons (CANCEL and OK). Cancel returns
to the previous rendering. OK also returns a new text if you entered it, but
if you leave the edit text in blank, it will return the initial rendering with
the original text.
the app started rendering a default fragment (the email fragment).
the option second group has a switch and it only renders a toast
talking about its status.
the way that the fragments handle the alertDialog are a little bit different.
uses the style to allow to use the top of the screen in a transparency way.
 */
public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener,
                                                                CompoundButton.OnCheckedChangeListener {
    DrawerLayout drawerLayout;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbar();
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navView);

        setFragmentByDefault();

        navigationView.setNavigationItemSelectedListener(this);

        Switch switchBtn = (Switch) navigationView.getMenu().findItem(R.id.switch_in_nav_options).getActionView();
        switchBtn.setOnCheckedChangeListener(this);
    }

    private void setToolbar() {
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        //inserting and rendering the hamburger icon.
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setFragmentByDefault() {
        changeFragment(new EmailFragment(), navigationView.getMenu().getItem(0));
    }

    private void changeFragment(Fragment fragment, MenuItem item) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
        item.setChecked(true);
        getSupportActionBar().setTitle(item.getTitle());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //open the navigation view (sidebar equivalent).
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        boolean fragmentTransaction = false;
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.menu_email:
                fragment = new EmailFragment();
                fragmentTransaction = true;
                break;
            case R.id.menu_alert:
                fragment = new AlertsFragment();
                fragmentTransaction = true;
                break;
            case R.id.menu_info:
                fragment = new InfoFragment();
                fragmentTransaction = true;
                break;
        }
        if (fragmentTransaction) {
            changeFragment(fragment, item);
            drawerLayout.closeDrawers();
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            Toast.makeText(MainActivity.this, "The option is checked", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "The option is unchecked", Toast.LENGTH_SHORT).show();
        }
    }
}