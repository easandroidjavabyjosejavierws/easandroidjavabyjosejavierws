package com.example.section_08_navigation_drawer_lab.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.section_08_navigation_drawer_lab.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class InfoFragment extends Fragment implements View.OnClickListener{
    private FloatingActionButton fab;

    public InfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        fab = view.findViewById(R.id.fab);
        //link with a listener to implement the fab dialog.
        fab.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Information");
        builder.setMessage("This alert dialog is just to show a normal informative message to the user, nothing to interact with");
        builder.setNeutralButton("Exit", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}