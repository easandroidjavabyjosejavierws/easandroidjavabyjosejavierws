package com.example.section_08_navigation_drawer_lab.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.section_08_navigation_drawer_lab.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class AlertsFragment extends Fragment implements View.OnClickListener, DialogInterface.OnClickListener {
    private FloatingActionButton fab;
    private TextView textViewTitle;
    private Switch switchBtn;

    public AlertsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alerts, container, false);

        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(this);

        textViewTitle = view.findViewById(R.id.textViewTitle);
        return view;
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Alerts");

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_fragment_dialog, null);

        switchBtn = dialogView.findViewById(R.id.switchDialog);

        builder.setView(dialogView);

        //set up buttons
        builder.setPositiveButton("OK", this);
        builder.setNegativeButton("Cancel", this);
        textViewTitle.setText(null);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            if (switchBtn.isChecked()) {
                textViewTitle.setText("Alerts Enabled");
            } else {
                textViewTitle.setText("Alerts Disabled");
            }
        } else if (which == DialogInterface.BUTTON_NEGATIVE) {
            dialog.cancel();
        }
    }
}