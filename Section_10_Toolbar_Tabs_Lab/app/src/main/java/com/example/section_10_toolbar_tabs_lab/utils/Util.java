package com.example.section_10_toolbar_tabs_lab.utils;

import android.content.Context;

import com.example.section_10_toolbar_tabs_lab.R;
import com.example.section_10_toolbar_tabs_lab.models.Country;

import java.util.ArrayList;
import java.util.List;

public class Util {
    public static List<Country> getCountries(final Context context) {
        return new ArrayList<Country>() {{
            add(new Country(context.getString(R.string.Spain_country), "ES"));
            add(new Country(context.getString(R.string.Argentina_country), "AR"));
            add(new Country(context.getString(R.string.Bolivia_country), "BO"));
            add(new Country(context.getString(R.string.Chile_country), "CL"));
            add(new Country(context.getString(R.string.Colombia_country), "CO"));
            add(new Country(context.getString(R.string.Ecuador_country), "EC"));
            add(new Country(context.getString(R.string.Mexico_country), "MX"));
            add(new Country(context.getString(R.string.Peru_country), "PE"));
            add(new Country(context.getString(R.string.Uruguay_country), "UY"));
            add(new Country(context.getString(R.string.Venezuela_country), "VE"));
        }};
    }
}
