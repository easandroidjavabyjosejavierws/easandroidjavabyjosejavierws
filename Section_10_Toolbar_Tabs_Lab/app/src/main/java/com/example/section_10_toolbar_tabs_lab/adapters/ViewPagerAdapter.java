package com.example.section_10_toolbar_tabs_lab.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.section_10_toolbar_tabs_lab.fragments.PersonFormFragment;
import com.example.section_10_toolbar_tabs_lab.fragments.PersonListFragment;

import static com.example.section_10_toolbar_tabs_lab.activities.MainActivity.PERSON_FORM_FRAGMENT;
import static com.example.section_10_toolbar_tabs_lab.activities.MainActivity.PERSON_LIST_FRAGMENT;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    int numberOfTabs;

    public ViewPagerAdapter(@NonNull FragmentManager fm, Context context, int numberOfTabs) {
        super(fm, numberOfTabs);
        this.numberOfTabs = numberOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case PERSON_FORM_FRAGMENT:
                return new PersonFormFragment();
            case PERSON_LIST_FRAGMENT:
                return new PersonListFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
