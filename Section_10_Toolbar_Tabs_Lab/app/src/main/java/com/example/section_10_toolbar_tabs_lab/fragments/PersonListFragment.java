package com.example.section_10_toolbar_tabs_lab.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.example.section_10_toolbar_tabs_lab.R;
import com.example.section_10_toolbar_tabs_lab.adapters.PersonAdapter;
import com.example.section_10_toolbar_tabs_lab.models.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonListFragment extends Fragment {
    private List<Person> persons;
    private ListView listView;
    private PersonAdapter adapter;

    public PersonListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_person_list, container, false);
        persons = new ArrayList<Person>();
        listView = view.findViewById(R.id.listViewPerson);
        adapter = new PersonAdapter(getContext(), R.layout.list_view_item_person, persons);
        listView.setAdapter(adapter);
        return view;
    }

    public void addPerson(Person person) {
        persons.add(person);
        adapter.notifyDataSetChanged();
    }
}