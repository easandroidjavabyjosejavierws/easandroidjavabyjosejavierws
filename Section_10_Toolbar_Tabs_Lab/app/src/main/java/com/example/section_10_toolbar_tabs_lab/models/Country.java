package com.example.section_10_toolbar_tabs_lab.models;
import java.text.MessageFormat;

public class Country {
    private String Name;
    private String CountryCode;

    //constructor.
    public Country(String name, String countryCode) {
        Name = name;
        CountryCode = countryCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getFlagURL() {
        return MessageFormat.format("https://www.countryflags.io/{0}/flat/64.png",
                getCountryCode());
    }

    /*important!
you need to override the toString(). This way, the spinner will use name to render
as the only available value from the drop-down list.
 */
    @Override
    public String toString() {
        return Name;
    }
}
