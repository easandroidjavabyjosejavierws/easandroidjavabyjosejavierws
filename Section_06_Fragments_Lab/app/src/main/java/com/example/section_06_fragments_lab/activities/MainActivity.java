package com.example.section_06_fragments_lab.activities;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.section_06_fragments_lab.R;
import com.example.section_06_fragments_lab.adapters.MailAdapter;
import com.example.section_06_fragments_lab.models.Mail;
import com.example.section_06_fragments_lab.utils.Util;

import java.util.List;

/*lesson 108, section 9.
    fragment lab.
    this version implements the listView with all of
    ui requirements but nothing with the fragments.
 */
public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private List<Mail> mails;
    private MailAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        mails = Util.getDummyData();

        //add listener to the listView using lambdas:
        listView.setOnItemClickListener((adapterView, view, position, id) -> {
            Toast.makeText(MainActivity.this, mails.get(position).getSenderName() + " was clicked.", Toast.LENGTH_LONG).show();
        });

        //works with a customized adapter.
        MailAdapter myAdapter = new MailAdapter(this, R.layout.list_view_mail, mails);
        listView.setAdapter(myAdapter);
    }
}