package com.example.section_01;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    /* covers lesson 34 section 4.
    this simply renders (uses Toast) a message
        working with the button property.
        the click on the button calls sayHi().
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sayHi(View v) {
        Toast.makeText(this, "button was clicked", Toast.LENGTH_LONG).show();
    }
}
