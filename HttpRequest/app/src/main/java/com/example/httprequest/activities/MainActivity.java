package com.example.httprequest.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequest.R;
import com.example.httprequest.models.City;

import org.json.JSONException;
import org.json.JSONObject;

/*
lesson 172 section 18.
parsing a json object in a native way (without libraries).

 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String json = "{\n" +
                      "id: 1,\n" +
                      "name: 'London'" +
                      "}";

        City city = null;
        try {
            JSONObject mJson = new JSONObject(json);
            int id = mJson.getInt("id");
            String name = mJson.getString("name");

            city = new City(id, name);
        } catch(JSONException e) {
            e.printStackTrace();
        }

        Toast.makeText(this, city.getId() + " -- " + city.getName(), Toast.LENGTH_LONG).show();
    }
}