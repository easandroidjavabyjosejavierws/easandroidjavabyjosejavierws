package com.example.httprequestG.activities;

import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequestG.API.API;
import com.example.httprequestG.API.APIServices.WeatherService;
import com.example.httprequestG.R;
import com.example.httprequestG.models.City;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
lesson 179, 180 section 18.
this is a copy of HttpRequestF.
Just refactored.
Retrieves temperature in celsius degrees.
included the Main object to parse the service main object (contains temperatures)
coming into the response.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WeatherService service = API.getApi().create(WeatherService.class);

        /* the getCity() retrieves temperatures in kelvin degrees
         */
        //Call<City> cityCall = service.getCity("Málaga, CO", API.APPKEY);

        /* the getCityCelsius() retrieves temperatures in celsius degrees
         */
        Call<City> cityCall = service.getCityCelsius("Málaga, CO", API.APPKEY, API.TEMP );
        //Call<City> cityCall = service.getCity("xyz", "45d0dd1b82ad291e225bc56bcf572db0");
        //at this point the request is prepared but not executed.
        cityCall.enqueue(new Callback<City>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (response.body() == null) {
                    String error = null;
                    try {
                        error = response.errorBody().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(MainActivity.this, "Request failed" + error, Toast.LENGTH_LONG).show();
                    finishAndRemoveTask();

                } else {
                    City city = response.body();
                    finishAndRemoveTask();
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Request failed", Toast.LENGTH_LONG).show();
            }
        });
    }
}