package com.example.httprequestG.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class City {
    private int id;
    private String name;
    private Main main;

    //constructors
    public City() {
    }

    public City(int id, String name, Main main) {
        this.id = id;
        this.name = name;
        this.main = main;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public static Main parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        Main main = gson.fromJson(response, Main.class);
        return main;
    }
}
