package com.example.httprequestD.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequestD.R;
import com.example.httprequestD.models.Town;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/*
lesson 175 section 18.
using Gson object from google and parsing
json with embedded objects (City is a Town attribute).

 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String json =
                "{" +
                    "id: 0," +
                    "city: {" +
                        "id: 8,"+
                        "name: 'Lake Worth'" +
                    "}" +
                "}";

        Gson gson = new GsonBuilder().create();
        Town town = gson.fromJson(json, Town.class);
        Toast.makeText(this, town.getCity().getId() + " -- " + town.getCity().getName(), Toast.LENGTH_LONG).show();
    }
}