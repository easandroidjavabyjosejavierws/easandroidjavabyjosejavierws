package com.example.myloginb.applications;

import android.app.Application;
import android.os.SystemClock;

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        /* this a trick to see the rendering of the
         splash screen. It lasts 3 seconds.
         */
        SystemClock.sleep(3000);
    }
}
