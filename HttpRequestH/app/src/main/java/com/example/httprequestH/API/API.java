package com.example.httprequestH.API;

import com.example.httprequestH.deserializers.MyDeserializer;
import com.example.httprequestH.models.City;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String APPKEY = "45d0dd1b82ad291e225bc56bcf572db0";
    public static final String TEMP = "metric";
    private static Retrofit retrofit = null;

    public static Retrofit getApi() {
        if (retrofit == null) {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(City.class, new MyDeserializer());

            retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(builder.create()))
                        .build();
        }
        return retrofit;
    }
}
