package com.example.httprequestH.models;

public class City {
    private int id;
    private String name;
    private String country;
    //private Main main;

    //constructors
    public City() {
    }

    public City(int id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
        //this.main = main;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
/*public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public static Main parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        Main main = gson.fromJson(response, Main.class);
        return main;
    }*/
}
