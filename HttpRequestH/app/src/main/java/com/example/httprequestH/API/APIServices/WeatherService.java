package com.example.httprequestH.API.APIServices;

import com.example.httprequestH.models.City;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("weather")
    Call<City> getCity(@Query("q") String city, @Query("appid") String key);

    @GET("weather")  //to get temperature in celsius.
    Call<City> getCityCelsius(@Query("q") String city, @Query("appid") String key, @Query("units") String value);
}
