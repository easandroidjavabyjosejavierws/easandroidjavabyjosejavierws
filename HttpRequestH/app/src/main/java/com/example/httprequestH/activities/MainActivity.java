package com.example.httprequestH.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.httprequestH.API.API;
import com.example.httprequestH.API.APIServices.WeatherService;
import com.example.httprequestH.R;
import com.example.httprequestH.models.City;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
lesson 181 section 18.
the parsing process has been changed.
That because country is nested inside of sys object,
and I don't want to create a second class for that.
the MyDeserializer class is the key.
the City object only needs to define the new attribute.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WeatherService service = API.getApi().create(WeatherService.class);

        /* the getCity() retrieves temperatures in kelvin degrees
         */
        //Call<City> cityCall = service.getCity("Málaga, CO", API.APPKEY);

        /* the getCityCelsius() retrieves temperatures in celsius degrees
         */
        Call<City> cityCall = service.getCityCelsius("Málaga, CO", API.APPKEY, API.TEMP );

        cityCall.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                City city = response.body();
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Request failed", Toast.LENGTH_LONG).show();
            }
        });
    }
}