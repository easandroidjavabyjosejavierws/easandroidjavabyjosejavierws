/* this project solves the problem with listView and buttons
when they are clicked.
 */
package com.example.section_02b2;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/*lesson 50, 51 section 5.
the idea is to create a linear layout with an image, a textView
and a custom adaptor.
 */
public class MainActivity extends AppCompatActivity {

    private ListView listView;
    List<String> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        //data to render is added to the listView
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando2");
        names.add("lina2");
        names.add("diana2");
        names.add("niko2");
        names.add("orlando3");
        names.add("lina3");
        names.add("diana3");
        names.add("niko3");

        //add a customized adapter.
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, names);
        listView.setAdapter(myAdapter);
//        Toast.makeText( this, "returned position " + myAdapter.currentPosition, Toast.LENGTH_SHORT).show();
    }
}