package com.games;

import com.badlogic.gdx.Game;

/*
lesson 207-212 Section 20.
multiple screens (using Game class).
Implement BaseScreen as an abstract class to be extended
by the additional screens.
Stage creation.
First actor creation (the main role).
Rendering the first actor.
Second actor creation (mr pinchos).
Collision working with bounding boxes. (mr. pinchos hits the player).
 */
public class MainGame extends Game {

    @Override
    public void create() {
        setScreen(new MainGameScreen(this));
    }
}
