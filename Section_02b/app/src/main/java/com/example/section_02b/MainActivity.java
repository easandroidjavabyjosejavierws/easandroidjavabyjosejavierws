package com.example.section_02b;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/*lesson 50, 51 section 5.
the idea is to create a linear layout with an image, a textView
and a custom adaptor.
 */
public class MainActivity extends AppCompatActivity {

    private ListView listView;
    List<String> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        //data to render is added to the listView
        names.add("orlando");
        names.add("lina");
        names.add("diana");
        names.add("niko");
        names.add("orlando2");
        names.add("lina2");
        names.add("diana2");
        names.add("niko2");
        names.add("orlando3");
        names.add("lina3");
        names.add("diana3");
        names.add("niko3");
        //add listener to the listView using lambdas:
        listView.setOnItemClickListener((adapterView, view, position, id) -> {
            Toast.makeText(MainActivity.this, names.get(position) + " was clicked.", Toast.LENGTH_LONG).show();
        });
        /* this version works with the regular anonymous class.
       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, names.get(position) + " was clicked.", Toast.LENGTH_LONG).show();
            }
        });*/

        //add a customized adapter.
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, names);
        listView.setAdapter(myAdapter);
    }
}