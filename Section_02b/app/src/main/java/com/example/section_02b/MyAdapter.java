package com.example.section_02b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<String> names;

    //constructor
    public MyAdapter(Context context, int layout, List<String> names) {
        this.context    = context;
        this.layout     = layout;
        this.names      = names;
    }

    @Override
    public int getCount() { //means: How many items are in the collection represented by this Adapter.
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        return names.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    /*The adapter needs to create a layout for each row of the list.
    this method runs for every row of our customized layout (list_item).
    The ListView instance calls the getView() method on the adapter
    for each data element. In this method the adapter creates
    the row layout and maps the data to the views in the layout.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*the coming view is duplicated.
         */
        View v = convertView;

        //the duplicated view is inflated with the customized layout (the linear list_item).
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        v = layoutInflater.inflate(R.layout.list_item, null);

        //retrieve the item based on its position.
        String currentName = names.get(position);
        //currentName = (String) getItem(position);

        //create a reference of the element (textView) and fill it.
        TextView textView = v.findViewById(R.id.textView);
        textView.setText(currentName);

        //return the view updated with my data
        return v;
    }
}
